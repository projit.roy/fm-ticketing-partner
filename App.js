import React, {Component} from 'react';
import {StatusBar, Platform, SafeAreaView} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import SplashScreen from './src/screens/SplashScreen';
import LoginNavigator from './src/routes/LoginNavigator';
import HomeScreenNavigator from './src/routes/HomeScreenNavigator';
import OrderHistoryNavigator from './src/routes/OrderHistoryNavigator';
import SaleHistoryNavigator from './src/routes/SaleHistoryNavigator';
import UpdateProfileNavigator from './src/routes/UpdateProfileNavigator';
import ReviewScreenSaleOne from './src/screens/ReviewScreenSaleOne';
import ReviewScreenSaleTwo from './src/screens/ReviewScreenSaleTwo';
import ReviewScreenBuyOne from './src/screens/ReviewScreenBuyOne';
import ReviewScreenBuyTwo from './src/screens/ReviewScreenBuyTwo';
import TermsCondition from './src/screens/TermsCondition';
import AboutUs from './src/screens/AboutUs';
import PrivacyPolicy from './src/screens/PrivacyPolicy';
import PaymentTerms from './src/screens/PaymentTerms';
import AboutTicket from './src/screens/AboutTicketPartner';
import Help from './src/screens/Help';
import Color from './src/constants/Color';

const Stack = createStackNavigator();

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <SafeAreaProvider>
        <NavigationContainer>
          <StatusBar
            //hidden={true}
            translucent
            barStyle="light-content"
          />
          <Stack.Navigator initialRouteName="SplashScreen">
            <Stack.Screen
              name="SplashScreen"
              options={{headerShown: false}}
              component={SplashScreen}
            />
            <Stack.Screen
              name="Login"
              component={LoginNavigator}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="homeScreen"
              component={HomeScreenNavigator}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="OrderHistory"
              component={OrderHistoryNavigator}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="SaleHistory"
              component={SaleHistoryNavigator}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="ReviewSaleOne"
              component={ReviewScreenSaleOne}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="ReviewSaleTwo"
              component={ReviewScreenSaleTwo}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="ReviewBuyOne"
              component={ReviewScreenBuyOne}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="ReviewBuyTwo"
              component={ReviewScreenBuyTwo}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="UpdateProfile"
              component={UpdateProfileNavigator}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Privacy"
              component={PrivacyPolicy}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="TermsCondition"
              component={TermsCondition}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Payment"
              component={PaymentTerms}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="AboutTicket"
              component={AboutTicket}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="AboutUs"
              component={AboutUs}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="Help"
              component={Help}
              options={{headerShown: false}}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </SafeAreaProvider>
    );
  }
}

export default App;
