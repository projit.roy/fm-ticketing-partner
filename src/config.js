const ENVIRONMENT = 'DEVELOPMENT';
//const ENVIRONMENT = 'PRODUCTION';
export const BASE_URL =
  ENVIRONMENT === 'DEVELOPMENT'
    ? 'https://aimlyticsjson-bcrm.magnaquest.com/RestApi'
    : '';
const usersNamespace = 'users';
const createUser = 'AddOperationalEntity';
const checkUser = 'CHECKUSERAVAILABILITY';
const generateOtp = 'GenerateOtp';
const validateOtp = 'ValidateOtp';
const authenticateUser = 'AuthenticateUser';
const dashboard = 'GetRecordsBySearch';
const forgot = 'SelfcareResetPassword';
const update = 'ModifyOperationalEntity';
// export const headers = {
//   Accept: 'application/json',
//   'Content-Type': 'application/json',
//   USERNAME: 'BUZZBYTES',
//   PASSWORD: 'BUZZBYTES123',
//   EXTERNALPARTY: 'MQS',
// };
export const loginUrl = `${BASE_URL}/${authenticateUser}`;
export const userCheck = `${BASE_URL}/${checkUser}`;
export const getOtp = `${BASE_URL}/${generateOtp}`;
export const validOtp = `${BASE_URL}/${validateOtp}`;
export const distributor = `${BASE_URL}/${dashboard}`;
export const forgotPass = `${BASE_URL}/${forgot}`;
export const create = `${BASE_URL}/${createUser}`;
export const modify = `${BASE_URL}/${update}`;
export const city = 'https://www.fridaymovies.in/api/v1/get-city?city=';
