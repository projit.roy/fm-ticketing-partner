export const fetchUtil = (url, method = 'GET', headers, body) =>
  fetch(url, {
    method,
    body: body ? body : undefined,
    headers,
  }).then(
    (response) =>
      new Promise((resolve) => {
        response.text().then((text) => {
          try {
            const json = JSON.parse(text);
            resolve({ok: response.ok, code: response.status, json});
          } catch (ex) {
            try {
              // eslint-disable-next-line no-undef
              const parser = new DOMParser();
              const xmlDoc = parser.parseFromString(text, 'text/xml');
              const title = xmlDoc.getElementsByTagName('title');
              let errorStr = title[0].childNodes[0].nodeValue;
              const p = xmlDoc.getElementsByTagName('p');
              errorStr += p[0].childNodes[0].nodeValue;
              resolve({
                ok: response.ok,
                code: response.status,
                json: {api_message: errorStr},
              });
            } catch (ex2) {
              resolve({
                ok: response.ok,
                code: response.status,
                json: {api_message: text},
              });
            }
          }
        });
      }),
  );
