import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';
import normalize from 'react-native-normalize';
import Icon from 'react-native-easy-icon';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Snackbar from 'react-native-snackbar';
import Spinner from 'react-native-loading-spinner-overlay';
import Color from '../constants/Color';
import {
  getInfo,
  modifyDistributor,
  getDistributorDetails,
} from '../screens/api';
import text from '../constants/strings';

export default class UpdateProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userName: '',
      phoneNumber: '',
      password: '',
      confirm: '',
      isTermsChecked: false,
      address: '',
      city: '',
      pincode: '',
      tradeName: '',
      aadhhar: '',
      gst: '',
      loader: false,
    };
  }

  componentDidMount() {
    this.fetchDetails();
  }

  fetchDetails = () => {
    this.setState({loader: true});
    AsyncStorage.getItem('session').then((value) => {
      let data = JSON.parse(value);
      const user = {
        KEY_NAMEVALUE: {
          KEY_NAME: 'PROCESS',
          KEY_VALUE: 'DISTRIBUTORDETAILS',
        },
        ADDITIONAL_INFO: JSON.stringify({USERNAME: data.USER_NAME}),
      };
      this.setState({
        userName: data.USER_DESCR,
        phoneNumber: data.MOBILE_NO,
      });
      getDistributorDetails(user).then((res) => {
        const user = {
          KEY_NAMEVALUE: {
            KEY_NAME: 'PROCESS',
            KEY_VALUE: 'DISTRIBUTORINFO',
          },
          ADDITIONAL_INFO: JSON.stringify({USERNAME: data.USER_NAME}),
        };
        console.log(res);
        if (res.code === 200) {
          let json = res.json.RESPONSEINFO;
          if (json.STATUS.ERRORNO === '0') {
            const info = json.DISTRIBUTORDETAILS;
            this.setState({
              ENTITYCODE: info.ENTITY_CODE,
              ENTITY_NAME: info.ENTITY_NAME,
            });
          }
        }
        getInfo(user).then((res) => {
          if (res.code === 200) {
            this.setState({loader: false});
            let json = res.json.RESPONSEINFO;
            if (json.STATUS.ERRORNO === '0') {
              const info = json.DISTRIBUTORINFO;
              this.setState({
                data: info,
                address: info.ADDRESS,
                city: info.CITY,
                pincode: info.PINCODE,
                tradeName: info.TRADE_NAME,
                aadhhar: info.PAN,
                gst: info.GST,
              });
            }
          }
        });
      });
    });
  };

  validate = () => {
    const {aadhhar, gst} = this.state;
    if (aadhhar !== '') {
      if (aadhhar.length !== 10) {
        Snackbar.show({
          text: text.panError,
          duration: Snackbar.LENGTH_SHORT,
        });
        return false;
      }
    }
    if (gst !== '') {
      if (gst.length !== 15) {
        Snackbar.show({
          text: text.gstError,
          duration: Snackbar.LENGTH_SHORT,
        });
        return false;
      }
    }
    return true;
  };

  update = () => {
    if (this.validate()) {
      this.setState({loader: true});
      const data = {
        OPERATIONALENTITYINFO: {
          ENTITYCODE: this.state.ENTITYCODE,
          ENTITYNAME: this.state.ENTITY_NAME,
          CONTACTINFO: {
            MOBILEPHONE: this.state.phoneNumber,
          },
          ADDRESSINFO: {
            ADDRESS1: this.state.address,
            CITY: this.state.city,
            ZIPCODE: this.state.pincode,
            COUNTRY: 'INDIA',
            COUNTRYCODE: 'IND',
          },
          FLEXATTRIBUTEINFO: {
            ATTRIBUTE1: this.state.gst,
            ATTRIBUTE2: this.state.aadhhar,
            ATTRIBUTE3: this.state.tradeName,
          },
        },
      };
      console.log(data);
      modifyDistributor(data).then((res) => {
        if (res.code === 200) {
          let json = res.json.RESPONSEINFO;
          if (json.STATUS.ERRORNO === '0') {
            this.setState({loader: false});
            setTimeout(
              () =>
                Snackbar.show({
                  text: text.dataUpdateSuccess,
                  duration: Snackbar.LENGTH_SHORT,
                }),
              500,
            );
            this.fetchDetails();
          }
        }
      });
    }
  };

  render() {
    return (
      <ScrollView contentContainerStyle={styles.container} bounces="false">
        <Spinner visible={this.state.loader} textContent="" />
        <View style={styles.container}>
          <ImageBackground
            source={require('../../assets/login_bg.png')}
            style={styles.image}>
            <View style={{padding: normalize(8)}}>
              <View style={{height: normalize(30)}} />
              <View
                style={{
                  width: normalize(100),
                  height: normalize(100),
                  alignSelf: 'center',
                }}>
                <Image
                  style={{width: normalize(100), height: normalize(100)}}
                  source={require('../../assets/ICON.png')}
                />
              </View>
              <View style={{height: normalize(30)}} />
              <View
                style={{
                  flexDirection: 'row',
                  height: normalize(50),
                  backgroundColor: Color.BACKGROUND_COLOR,
                  borderRadius: normalize(50) / 2,
                }}>
                <Icon
                  style={{alignSelf: 'center', margin: 10}}
                  name={'account'}
                  type="material-community"
                  size={normalize(20)}
                  color={Color.ICON_COLOR}
                />
                <TextInput
                  editable={false}
                  style={{flex: 1, color: 'white'}}
                  placeholder={text.placeholderName}
                  placeholderTextColor={Color.ICON_COLOR}
                  value={this.state.userName}
                  onChange={(text) => this.setState({userName: text})}
                />
              </View>
              <View
                style={{
                  marginTop: normalize(10),
                  flexDirection: 'row',
                  height: normalize(50),
                  backgroundColor: Color.BACKGROUND_COLOR,
                  borderRadius: normalize(50) / 2,
                }}>
                <Icon
                  style={{alignSelf: 'center', margin: 10}}
                  name={'phone'}
                  type="material-community"
                  size={normalize(20)}
                  color={Color.ICON_COLOR}
                />
                <TextInput
                  editable={false}
                  style={{flex: 1, color: 'white'}}
                  placeholder={text.placeholderPhone}
                  placeholderTextColor={Color.ICON_COLOR}
                  value={this.state.phoneNumber}
                  onChange={(text) => this.setState({phoneNumber: text})}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  height: 50,
                  backgroundColor: Color.BACKGROUND_COLOR,
                  borderRadius: 25,
                  marginTop: 5,
                }}>
                <Icon
                  style={{alignSelf: 'center', margin: 10}}
                  name={'map-marker'}
                  type="material-community"
                  size={20}
                  color={Color.ICON_COLOR}
                />
                <TextInput
                  style={{flex: 1, color: 'white'}}
                  placeholder={text.placeholderAddress}
                  onChangeText={(text) => this.setState({address: text})}
                  placeholderTextColor={Color.ICON_COLOR}
                  value={this.state.address}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  height: 50,
                  backgroundColor: Color.BACKGROUND_COLOR,
                  borderRadius: 25,
                  marginTop: 5,
                }}>
                <Icon
                  style={{alignSelf: 'center', margin: 10}}
                  name={'map-marker'}
                  type="material-community"
                  size={20}
                  color={Color.ICON_COLOR}
                />
                <TextInput
                  style={{flex: 1, color: 'white'}}
                  onChangeText={(text) => this.setState({city: text})}
                  placeholder={text.placeholderCity}
                  placeholderTextColor={Color.ICON_COLOR}
                  value={this.state.city}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  height: 50,
                  backgroundColor: Color.BACKGROUND_COLOR,
                  borderRadius: 50,
                  marginTop: 5,
                }}>
                <Icon
                  style={{alignSelf: 'center', margin: 10}}
                  name={'map-marker'}
                  type="material-community"
                  size={20}
                  color={Color.ICON_COLOR}
                />
                <TextInput
                  style={{flex: 1, color: 'white'}}
                  onChangeText={(text) => this.setState({pincode: text})}
                  placeholder={text.placeholderPincode}
                  keyboardType="number-pad"
                  maxLength={6}
                  placeholderTextColor={Color.ICON_COLOR}
                  value={this.state.pincode}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  height: 50,
                  backgroundColor: Color.BACKGROUND_COLOR,
                  borderRadius: 25,
                  marginTop: 5,
                }}>
                <Icon
                  style={{alignSelf: 'center', margin: 10}}
                  name={'account-box'}
                  type="material-community"
                  size={20}
                  color={Color.ICON_COLOR}
                />
                <TextInput
                  autoCapitalize="characters"
                  style={{flex: 1, color: 'white'}}
                  placeholder={text.placeholderTrade}
                  onChangeText={(text) => this.setState({tradeName: text})}
                  placeholderTextColor={Color.ICON_COLOR}
                  value={this.state.tradeName}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  height: 50,
                  backgroundColor: Color.BACKGROUND_COLOR,
                  borderRadius: 25,
                  marginTop: 5,
                }}>
                <Icon
                  style={{alignSelf: 'center', margin: 10}}
                  name={'account-box'}
                  type="material-community"
                  size={20}
                  color={Color.ICON_COLOR}
                />
                <TextInput
                  autoCapitalize="characters"
                  style={{flex: 1, color: 'white'}}
                  placeholder={text.placeholderPan}
                  onChangeText={(text) => this.setState({aadhhar: text})}
                  placeholderTextColor={Color.ICON_COLOR}
                  value={this.state.aadhhar}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  height: 50,
                  backgroundColor: Color.BACKGROUND_COLOR,
                  borderRadius: 25,
                  marginTop: 5,
                }}>
                <Icon
                  style={{alignSelf: 'center', margin: 10}}
                  name={'account-box'}
                  type="material-community"
                  size={20}
                  color={Color.ICON_COLOR}
                />
                <TextInput
                  autoCapitalize="characters"
                  style={{flex: 1, color: 'white'}}
                  placeholder={text.placeholderGst}
                  onChangeText={(text) => this.setState({gst: text})}
                  placeholderTextColor={Color.ICON_COLOR}
                  value={this.state.gst}
                />
              </View>
              <View style={{height: normalize(50)}} />
              <View
                style={{
                  flexDirection: 'row',
                  height: normalize(50),
                  alignContent: 'center',
                  alignItems: 'center',
                  backgroundColor: Color.BACKGROUND_COLOR,
                  borderRadius: normalize(50) / 2,
                }}>
                <TouchableOpacity
                  style={{flex: 1}}
                  onPress={() => this.update()}>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 16,
                      alignSelf: 'center',
                    }}>
                    {text.update}
                  </Text>
                </TouchableOpacity>
              </View>

              <View style={{height: normalize(30)}} />
            </View>
          </ImageBackground>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  checkBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  upperPortion: {
    margin: 10,
  },
  bottomPortion: {
    margin: 10,

    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
