import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
} from 'react-native';
import Icon from 'react-native-easy-icon';
import Color from '../constants/Color';
import normalize from 'react-native-normalize';
import Snackbar from 'react-native-snackbar';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Spinner from 'react-native-loading-spinner-overlay';
import {generateOtp, validateOtp} from '../screens/api';
import text from '../constants/strings';

export default class VerifyOtp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      code: '',
      enterPhoneNumber: '',
      timer: 30,
      isTimeRunning: false,
      isValidateApi: false,
      resendPasswordApi: false,
      phoneNumber: props.route.params.params.phoneNumber,
      loader: false,
    };
  }

  componentDidMount() {}

  async validateOtpApi() {
    let otp = this.state.code;
    console.log(otp, this.state.phoneNumber);
    if (otp.length < 4) {
      Snackbar.show({
        text: text.otpError,
        duration: Snackbar.LENGTH_SHORT,
      });
    } else {
      this.setState({loader: true});
      let data = {
        VALIDATEOTP: {
          MOBILEPHONE: this.state.phoneNumber,
          OTPEMAIL: '',
          OTP: otp,
        },
      };
      const res = await validateOtp(data);
      console.log(res);
      if (res.code === 200) {
        this.setState({loader: false});
        let json = res.json.RESPONSEINFO;
        if (json.STATUS.ERRORNO === '0') {
          this.props.navigation.navigate(
            this.props.route.params.params.screen,
            {otp: 'verified'},
          );
        } else if (json.STATUS.ERRORNO === '110521') {
          Snackbar.show({
            text: text.code110521,
            duration: Snackbar.LENGTH_SHORT,
          });
        } else if (json.STATUS.ERRORNO === '40111') {
          Snackbar.show({
            text: text.code40111,
            duration: Snackbar.LENGTH_SHORT,
          });
        } else {
          Snackbar.show({
            text: text.somethingWrong,
            duration: Snackbar.LENGTH_SHORT,
          });
        }
      }
    }
  }

  async resendCode() {
    this.setState({loader: true});
    this.setState({resendPasswordApi: true});
    let data = {
      GENERATEOTP: {
        MOBILEPHONE: this.state.phoneNumber,
        OTPEMAIL: '',
        PARTYID: '0',
        COUNTRYCODE: '',
        RESEND: 'TRUE',
        ATTRIBUTE1: '',
      },
    };
    console.log(data);
    const res = await generateOtp(data);
    console.log(res);
    if (res.code === 200) {
      this.setState({loader: false});
      let json = res.json.RESPONSEINFO;
      if (json.STATUS.ERRORNO === '0') {
        Snackbar.show({
          text: text.otpResent,
          duration: Snackbar.LENGTH_SHORT,
        });
      } else if (json.STATUS.ERRORNO === '110521') {
        Snackbar.show({
          text: text.code110521,
          duration: Snackbar.LENGTH_SHORT,
        });
      } else {
        Snackbar.show({
          text: text.somethingWrong,
          duration: Snackbar.LENGTH_SHORT,
        });
      }
    }
  }

  render() {
    return (
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <Spinner visible={this.state.loader} textContent="" />
        <SafeAreaView style={styles.container}>
          <View style={styles.container}>
            <ImageBackground
              source={require('../../assets/login_bg.png')}
              style={styles.image}>
              <View style={styles.upperPortion}>
                <TouchableOpacity
                  style={{padding: 8}}
                  onPress={() => this.props.navigation.goBack()}>
                  <Icon
                    name={'arrow-left'}
                    type="material-community"
                    size={24}
                    color="white"
                  />
                </TouchableOpacity>
                <View
                  style={{
                    justifyContent: 'center',
                    flex: 1,
                    alignItems: 'center',
                  }}>
                  <Image
                    style={{width: 100, height: 100, marginBottom: 10}}
                    source={require('../../assets/ICON.png')}
                  />
                </View>
                <Text style={{color: Color.ICON_COLOR, alignSelf: 'center'}}>
                  {text.placeholderOtp}
                </Text>
              </View>
              <View style={styles.bottomPortion}>
                <OTPInputView
                  style={{width: '60%', height: 100, alignSelf: 'center'}}
                  pinCount={4}
                  // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                  // onCodeChanged = {code => { this.setState({code})}}
                  // autoFocusOnLoad
                  codeInputFieldStyle={styles.underlineStyleBase}
                  codeInputHighlightStyle={styles.underlineStyleHighLighted}
                  onCodeFilled={(code) => {
                    this.setState({code: code});
                  }}
                />
                <Text
                  style={{color: Color.ICON_COLOR, marginTop: normalize(10)}}>
                  {text.otpMessage}
                </Text>
                <Text
                  style={{
                    color: 'white',
                    fontWeight: 'bold',
                    fontSize: normalize(18),
                  }}>
                  {this.state.enterPhoneNumber}
                </Text>
                <View style={{height: normalize(60)}} />
                <View
                  style={{
                    flexDirection: 'row',
                    height: 50,
                    alignContent: 'center',
                    alignItems: 'center',
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: 50 / 2,
                    marginTop: 15,
                  }}>
                  <TouchableOpacity
                    style={{flex: 1}}
                    onPress={() => {
                      this.validateOtpApi();
                    }}>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: 16,
                        alignSelf: 'center',
                      }}>
                      {text.confirm}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{width: normalize(10)}} />
                <View
                  style={{
                    flexDirection: 'row',
                    height: 50,
                    alignContent: 'center',
                    alignItems: 'center',
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: 50 / 2,
                    marginTop: 15,
                  }}>
                  <TouchableOpacity
                    disabled={this.state.isTimeRunning}
                    style={{flex: 1}}
                    onPress={() => {
                      this.resendCode();
                    }}>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: 16,
                        alignSelf: 'center',
                      }}
                      accessibilityState={{disabled: this.state.isTimeRunning}}>
                      {text.resend}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{height: 60}} />
              </View>
            </ImageBackground>
          </View>
        </SafeAreaView>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  upperPortion: {
    margin: 10,
    flex: 1,
  },
  bottomPortion: {
    margin: 10,
    flex: 2,

    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  borderStyleBase: {
    width: 50,
    height: 45,
  },

  borderStyleHighLighted: {
    borderColor: Color.BACKGROUND_COLOR,
  },

  underlineStyleBase: {
    width: 45,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
    fontSize: 20,
    //backgroundColor: Color.BACKGROUND_COLOR,
  },

  underlineStyleHighLighted: {
    borderColor: 'white',
  },
});
