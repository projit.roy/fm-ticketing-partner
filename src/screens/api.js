import {
  //headers,
  getOtp,
  validOtp,
  distributor,
  userCheck,
  forgotPass,
  create,
  modify,
  city,
} from '../config';
import {fetchUtil} from '../util';
import UUIDGenerator from 'react-native-uuid-generator';
import AsyncStorage from '@react-native-async-storage/async-storage';
import base64 from 'react-native-base64';

const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
  USERNAME: 'BUZZBYTES',
  PASSWORD: 'BUZZBYTES123',
  EXTERNALPARTY: 'MQS',
};

const getUserNamePass = async () => {
  await AsyncStorage.getItem('session').then((value) => {
    if (value !== null) {
      let data = JSON.parse(value);
      headers.USERNAME = data.USER_NAME;
      headers.PASSWORD = base64.decode(data.PASSWORD);
    }
  });
};

export const checkUser = async (data) => {
  let id = new Date().getTime();
  await UUIDGenerator.getRandomUUID().then((uuid) => {
    id = uuid;
  });
  const url = `${userCheck}?ReferenceNo=${id}`;
  console.log(url);
  return fetchUtil(url, 'Post', headers, JSON.stringify(data));
};

export const generateOtp = async (data) => {
  let id = new Date().getTime();
  await UUIDGenerator.getRandomUUID().then((uuid) => {
    id = uuid;
  });
  const url = `${getOtp}?ReferenceNo=${id}`;
  console.log(url);
  return fetchUtil(url, 'Post', headers, JSON.stringify(data));
};

export const validateOtp = async (data) => {
  let id = new Date().getTime();
  await UUIDGenerator.getRandomUUID().then((uuid) => {
    id = uuid;
  });
  const url = `${validOtp}?ReferenceNo=${id}`;
  console.log(url);
  return fetchUtil(url, 'Post', headers, JSON.stringify(data));
};

export const getDistributorDashboard = async (data) => {
  let id = new Date().getTime();
  await getUserNamePass();
  await UUIDGenerator.getRandomUUID().then((uuid) => {
    id = uuid;
  });
  const url = `${distributor}?ReferenceNo=${id}`;
  console.log(url);
  console.log(headers);
  return fetchUtil(url, 'Post', headers, JSON.stringify(data));
};

export const getAssets = async (data) => {
  let id = new Date().getTime();
  await getUserNamePass();
  await UUIDGenerator.getRandomUUID().then((uuid) => {
    id = uuid;
  });
  const url = `${distributor}?ReferenceNo=${id}`;
  console.log(url, headers);
  return fetchUtil(url, 'Post', headers, JSON.stringify(data));
};

export const getSales = async (data) => {
  let id = new Date().getTime();
  await getUserNamePass();
  await UUIDGenerator.getRandomUUID().then((uuid) => {
    id = uuid;
  });
  const url = `${distributor}?ReferenceNo=${id}`;
  console.log(url);
  return fetchUtil(url, 'Post', headers, JSON.stringify(data));
};

export const getOrders = async (data) => {
  let id = new Date().getTime();
  await getUserNamePass();
  await UUIDGenerator.getRandomUUID().then((uuid) => {
    id = uuid;
  });
  const url = `${distributor}?ReferenceNo=${id}`;
  console.log(url);
  return fetchUtil(url, 'Post', headers, JSON.stringify(data));
};

export const getSold = async (data) => {
  let id = new Date().getTime();
  await getUserNamePass();
  await UUIDGenerator.getRandomUUID().then((uuid) => {
    id = uuid;
  });
  const url = `${distributor}?ReferenceNo=${id}`;
  console.log(url);
  return fetchUtil(url, 'Post', headers, JSON.stringify(data));
};

export const getDistributorDetails = async (data) => {
  let id = new Date().getTime();
  await getUserNamePass();
  await UUIDGenerator.getRandomUUID().then((uuid) => {
    id = uuid;
  });
  const url = `${distributor}?ReferenceNo=${id}`;
  console.log(url);
  return fetchUtil(url, 'Post', headers, JSON.stringify(data));
};

export const forgotPassword = async (data) => {
  let id = new Date().getTime();
  //await getUserNamePass();
  await UUIDGenerator.getRandomUUID().then((uuid) => {
    id = uuid;
  });
  const url = `${forgotPass}?ReferenceNo=${id}`;
  console.log(url);
  return fetchUtil(url, 'Post', headers, JSON.stringify(data));
};

export const createUser = async (data) => {
  let id = new Date().getTime();
  await UUIDGenerator.getRandomUUID().then((uuid) => {
    id = uuid;
  });
  const url = `${create}?ReferenceNo=${id}`;
  console.log(url);
  return fetchUtil(url, 'Post', headers, JSON.stringify(data));
};

export const getInfo = async (data) => {
  let id = new Date().getTime();
  await getUserNamePass();
  await UUIDGenerator.getRandomUUID().then((uuid) => {
    id = uuid;
  });
  const url = `${distributor}?ReferenceNo=${id}`;
  console.log(url);
  return fetchUtil(url, 'Post', headers, JSON.stringify(data));
};

export const modifyDistributor = async (data) => {
  let id = new Date().getTime();
  await getUserNamePass();
  await UUIDGenerator.getRandomUUID().then((uuid) => {
    id = uuid;
  });
  const url = `${modify}?ReferenceNo=${id}`;
  console.log(url);
  return fetchUtil(url, 'Post', headers, JSON.stringify(data));
};

export const sale = async (data) => {
  let id = new Date().getTime();
  await getUserNamePass();
  await UUIDGenerator.getRandomUUID().then((uuid) => {
    id = uuid;
  });
  const url = `${distributor}?ReferenceNo=${id}`;
  console.log(url);
  return fetchUtil(url, 'Post', headers, JSON.stringify(data));
};

export const movieList = async (data) => {
  let id = new Date().getTime();
  await getUserNamePass();
  await UUIDGenerator.getRandomUUID().then((uuid) => {
    id = uuid;
  });
  const url = `${distributor}?ReferenceNo=${id}`;
  console.log(url);
  return fetchUtil(url, 'Post', headers, JSON.stringify(data));
};

export const movieBooking = async (data) => {
  let id = new Date().getTime();
  await getUserNamePass();
  await UUIDGenerator.getRandomUUID().then((uuid) => {
    id = uuid;
  });
  const url = `${distributor}?ReferenceNo=${id}`;
  console.log(url);
  return fetchUtil(url, 'Post', headers, JSON.stringify(data));
};

export const getCity = async (data) => {
  console.log(`${city}${data}`);
  return fetchUtil(`${city}${data}`, 'GET', {});
};
