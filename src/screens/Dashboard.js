import React, {Component} from 'react';
import {
  FlatList,
  ImageBackground,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
} from 'react-native';
import normalize from 'react-native-normalize';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import Color from '../constants/Color';
import {getDistributorDashboard} from '../screens/api';
import text from '../constants/strings';

const Item = ({
  MOVIE,
  PURCHASEDQUANTITY,
  SOLDQUANTITY,
  AVAILABLEQUANTITY,
  pur_date,
}) => (
  <View style={styles.item}>
    <View
      style={{
        marginStart: normalize(8),
        backgroundColor: Color.BACKGROUND_COLOR,
        borderRadius: normalize(5),
        padding: normalize(8),
      }}>
      <Text style={styles.title}>{MOVIE}</Text>
      <Text style={styles.subHeading}>{'(' + MOVIE + ')'}</Text>
      <View style={{flex: 1, flexDirection: 'row'}}>
        <View style={{flex: 1.3}}>
          <Text style={{fontSize: normalize(14), color: 'white'}}>
            {text.purchased + PURCHASEDQUANTITY + text.ticket}
          </Text>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>
              {text.purchasedDate}
            </Text>
            <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>
              {' ' + pur_date}
            </Text>
          </View>
        </View>
        <View style={{flex: 1}}>
          <View style={{flex: 1, flexDirection: 'row', alignSelf: 'flex-end'}}>
            <Text style={{fontSize: normalize(14), color: 'white'}}>
              {text.sold}
            </Text>
            <Text
              style={{
                fontSize: normalize(14),
                color: 'red',
                marginStart: 3,
                marginEnd: 3,
              }}>
              {' ' + SOLDQUANTITY}
            </Text>
            <Text style={{fontSize: normalize(14), color: 'white'}}>
              {text.ticket}
            </Text>
          </View>
          <View style={{flex: 1, flexDirection: 'row', alignSelf: 'flex-end'}}>
            <Text style={{fontSize: normalize(14), color: 'white'}}>
              {text.balance}
            </Text>
            <Text
              style={{fontSize: normalize(14), color: 'green', marginEnd: 3}}>
              {' ' + AVAILABLEQUANTITY + ' '}
            </Text>
            <Text style={{fontSize: normalize(14), color: 'white'}}>
              {text.ticket}
            </Text>
          </View>
        </View>
      </View>
    </View>
  </View>
);

export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      shouldRefresh: false,
    };
  }
  componentDidMount() {
    this.getDetails();
  }

  getDetails = () => {
    this.setState({loader: true});
    AsyncStorage.getItem('session').then(async (value) => {
      if (value !== null) {
        const data = JSON.parse(value);
        const user = {
          KEY_NAMEVALUE: {
            KEY_NAME: 'PROCESS',
            KEY_VALUE: 'DISTRIBUTORDASHBOARDDTLS',
          },
          ADDITIONAL_INFO: JSON.stringify({DISTRIBUTORID: data.REF_ID}),
        };
        const res = await getDistributorDashboard(user);
        console.log(res);
        if (res.code === 200) {
          console.log(
            res.json.RESPONSEINFO.DISTRIBUTORDASHBOARDDETAILS
              ? res.json.RESPONSEINFO.DISTRIBUTORDASHBOARDDETAILS
              : [],
          );
          this.setState({loader: false});
          this.setState({
            data: res.json.RESPONSEINFO.DISTRIBUTORDASHBOARDDETAILS
              ? res.json.RESPONSEINFO.DISTRIBUTORDASHBOARDDETAILS
              : [],
          });
        }
      }
    });
  };

  renderItem = ({item}) => (
    <Item
      MOVIE={item.MOVIE}
      PURCHASEDQUANTITY={item.PURCHASEDQUANTITY}
      SOLDQUANTITY={item.SOLDQUANTITY}
      AVAILABLEQUANTITY={item.AVAILABLEQUANTITY}
      pur_date={item.pur_date}
    />
  );
  EmptyListMessage = () => {
    return this.state.data === null || this.state.data.length === 0 ? (
      <Text style={styles.emptyListStyle}>{text.noData}</Text>
    ) : null;
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Spinner visible={this.state.loader} textContent="" />
        <ImageBackground
          source={require('../../assets/login_bg.png')}
          style={styles.image}>
          <View style={{flex: 1}}>
            <Text
              style={{
                color: 'white',
                alignSelf: 'center',
                fontWeight: 'bold',
                fontSize: normalize(20),
                margin: normalize(8),
              }}>
              {text.dashboard}
            </Text>
            <FlatList
              extraData={this.state.shouldRefresh}
              data={this.state.data}
              renderItem={this.renderItem}
              ListEmptyComponent={this.EmptyListMessage()}
            />
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerText: {
    padding: normalize(10),
    height: normalize(40),
  },
  emptyListStyle: {
    padding: 10,
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },

  item: {
    backgroundColor: Color.APP_YELLOW,
    borderRadius: 5,

    marginVertical: 8,
    marginHorizontal: 8,
  },
  title: {
    fontSize: normalize(20),
    color: 'white',
  },
  subHeading: {
    fontSize: normalize(14),
    color: Color.ICON_COLOR,
  },
});
