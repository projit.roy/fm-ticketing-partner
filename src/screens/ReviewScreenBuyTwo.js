import React, {Component} from 'react';
import {
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform,
} from 'react-native';
import Icon from 'react-native-easy-icon';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Snackbar from 'react-native-snackbar';
import Color from '../constants/Color';
import normalize from 'react-native-normalize';
import Spinner from 'react-native-loading-spinner-overlay';
import {sale} from '../screens/api';
import text from '../constants/strings';

export default class ReviewScreenSaleTwo extends Component {
  constructor(props) {
    super(props);
    console.log(props.route.params);
    let split = props.route.params.additional[0].split(':');
    this.state = {
      movieName: props.route.params.movie,
      movieGenere: props.route.params.genere,
      expiry: props.route.params.expiry,
      saleQuantity: props.route.params.quantity,
      price: split[1],
      phoneNumber: props.route.params.phoneNumber,
      selectedQuantity: '5',
      totalPrice: split[4],
      gst: split[2],
      charge: split[3],
    };
  }

  saleTicket = () => {
    AsyncStorage.getItem('session').then((value) => {
      let id = JSON.parse(value);
      const data = {
        KEY_NAMEVALUE: {
          KEY_NAME: 'PROCESS',
          KEY_VALUE: 'DISTRIBUTORSELLREQUEST',
        },
        ADDITIONAL_INFO: JSON.stringify({
          DISTRIBUTORID: id.USER_ID,
          MOVIE_NAME: this.state.movieName,
          CUSTOMER_MOBILE_NUMBER: this.state.phoneNumber,
          QUANTITY: this.state.saleQuantity,
          TOTAL_PRICE:
            Number(this.state.saleQuantity) * Number(this.state.price),
        }),
      };
      console.log(data);
      sale(data).then((res) => {
        console.log(res);
        if (res.code === 200) {
          let json = res.json.RESPONSEINFO;
          if (json.STATUS.ERRORNO === '0') {
            Snackbar.show({
              text: text.ticketBuySucess,
              duration: Snackbar.LENGTH_SHORT,
            });
            this.props.navigation.goBack();
          }
        }
      });
    });
  };

  render() {
    return (
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <Spinner visible={this.state.loader} textContent="" />
        <View style={styles.container}>
          <ImageBackground
            source={require('../../assets/asset-12.png')}
            style={styles.image}>
            <View style={{flex: 1}}>
              <View style={styles.upperPortion}>
                <TouchableOpacity
                  style={{marginTop: Platform.OS === 'ios' ? 20 : 25}}
                  onPress={() => {
                    this.props.navigation.goBack();
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      alignSelf: 'flex-start',
                    }}>
                    <Icon
                      name={'chevron-back'}
                      type="ionicon"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View
                  style={{
                    justifyContent: 'center',
                    flex: 1,
                    alignItems: 'center',
                  }}
                />
              </View>
              <View style={styles.bottomPortion}>
                <View style={styles.item}>
                  <View
                    style={{
                      marginStart: normalize(8),
                      backgroundColor: Color.BACKGROUND_COLOR,
                      borderRadius: normalize(5),
                      padding: normalize(8),
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        flex: 1,
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.title}>{this.state.movieName}</Text>
                    </View>
                    <Text style={styles.subHeading}>
                      {this.state.movieGenere}
                    </Text>
                    <Text style={styles.subHeading}>
                      {text.expiryDate}
                      {'\t'}
                      {this.state.expiry}
                    </Text>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <View style={{flex: 1}}>
                        <View style={{flex: 1, flexDirection: 'row'}}>
                          <Text
                            style={{fontSize: normalize(16), color: 'white'}}>
                            {text.ticketQuantity}
                          </Text>
                          <Text
                            style={{fontSize: normalize(16), color: 'white'}}>
                            {this.state.saleQuantity}
                          </Text>
                        </View>
                      </View>
                      <View>
                        <View
                          style={{
                            flex: 1,
                            alignSelf: 'flex-end',
                          }}>
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                              justifyContent: 'flex-end',
                            }}>
                            <Text
                              style={{
                                fontSize: normalize(16),
                                color: 'white',
                              }}>
                              {text.totalPrice}
                            </Text>
                            <View style={{width: normalize(5)}} />
                            <Text
                              style={{
                                fontSize: normalize(16),
                                color: 'white',
                              }}>
                              ₹ {this.state.price}
                            </Text>
                          </View>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            justifyContent: 'flex-end',
                            flexDirection: 'row',
                          }}>
                          <Text
                            style={{
                              fontSize: normalize(14),
                              color: 'white',
                            }}>
                            {text.gst}
                          </Text>
                          <View style={{width: normalize(5)}} />
                          <Text
                            style={{
                              fontSize: normalize(14),
                              color: 'white',
                            }}>
                            ₹ {this.state.gst}
                          </Text>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            justifyContent: 'flex-end',
                            flexDirection: 'row',
                          }}>
                          <Text
                            style={{
                              fontSize: normalize(14),
                              color: 'white',
                            }}>
                            {text.charges}
                          </Text>
                          <View style={{width: normalize(5)}} />
                          <Text
                            style={{
                              fontSize: normalize(14),
                              color: 'white',
                            }}>
                            ₹ {this.state.charge}
                          </Text>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            justifyContent: 'flex-end',
                            flexDirection: 'row',
                            marginTop: 5,
                          }}>
                          <Text
                            style={{
                              fontSize: normalize(17),
                              color: 'white',
                            }}>
                            {text.totalTicketPrice}
                          </Text>
                          <View style={{width: normalize(5)}} />
                          <Text
                            style={{
                              fontSize: normalize(17),
                              color: 'white',
                            }}>
                            ₹ {this.state.totalPrice}
                          </Text>
                        </View>
                        <View
                          style={{
                            flex: 1,
                            justifyContent: 'flex-end',
                            flexDirection: 'row',
                            marginTop: 10,
                          }}>
                          <Text
                            style={{
                              fontSize: normalize(19),
                              color: Color.APP_YELLOW,
                            }}>
                            {text.totalPrice}
                          </Text>
                          <View style={{width: normalize(5)}} />
                          <Text
                            style={{
                              fontSize: normalize(19),
                              color: Color.APP_YELLOW,
                            }}>
                            ₹ {this.state.totalPrice}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              <View
                style={{
                  position: 'absolute',
                  bottom: 0,
                  width: '100%',
                }}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    height: normalize(55),
                    justifyContent: 'center',
                    backgroundColor: 'green',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: normalize(20),
                    }}>
                    {text.totalPay}
                  </Text>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: normalize(24),
                    }}>
                    {'\t'}₹{this.state.totalPrice}
                  </Text>
                </View>
                <TouchableOpacity
                  style={{
                    bottom: 0,
                    width: '100%',
                  }}
                  onPress={() => {
                    //this.saleTicket();
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      height: normalize(65),
                      justifyContent: 'center',
                      backgroundColor: Color.BACKGROUND_COLOR,
                    }}>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(20),
                        alignSelf: 'center',
                      }}>
                      {text.orderNow}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </ImageBackground>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  upperPortion: {
    margin: normalize(10),
    flex: 1,
  },
  bottomPortion: {
    margin: normalize(10),
    flex: 2,
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  item: {
    backgroundColor: Color.APP_YELLOW,
    borderRadius: 5,

    marginVertical: 8,
    marginHorizontal: 8,
  },
  title: {
    flex: 1,
    fontSize: normalize(22),
    fontWeight: 'bold',
    color: Color.APP_YELLOW,
    marginRight: 50,
  },
  subHeading: {
    fontSize: normalize(14),
    color: 'grey',
  },
});
