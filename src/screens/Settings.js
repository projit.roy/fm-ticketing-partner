import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
} from 'react-native';
import normalize from 'react-native-normalize';
import Icon from 'react-native-easy-icon';
import AsyncStorage from '@react-native-async-storage/async-storage';
import text from '../constants/strings';

export default class Settings extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  logOut = () => {
    AsyncStorage.removeItem('session');
    this.props.navigation.reset({
      index: 0,
      routes: [{name: 'Login'}],
    });
  };

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <ImageBackground
            source={require('../../assets/login_bg.png')}
            style={styles.image}>
            <ScrollView style={styles.container}>
              <View style={styles.bottomPortion}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('OrderHistory');
                  }}>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Image source={require('../../assets/order_history.png')} />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      {text.orderHistory}
                    </Text>
                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />
                <View
                  style={{
                    height: 1,
                    backgroundColor: 'lightslategrey',
                    width: '100%',
                  }}
                />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('SaleHistory');
                  }}>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Icon
                      name={'share-square'}
                      type="font-awesome5"
                      size={normalize(26)}
                      color="white"
                    />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      {text.sellHistory}
                    </Text>
                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />
                <View
                  style={{
                    height: 1,
                    backgroundColor: 'lightslategrey',
                    width: '100%',
                  }}
                />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('UpdateProfile');
                  }}>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Image source={require('../../assets/account.png')} />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      {text.myAccount}
                    </Text>
                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />
                <View
                  style={{
                    height: 1,
                    backgroundColor: 'lightslategrey',
                    width: '100%',
                  }}
                />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('TermsCondition');
                  }}>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Image source={require('../../assets/terms.png')} />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      {text.terms}
                    </Text>
                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />
                <View
                  style={{
                    height: 1,
                    backgroundColor: 'lightslategrey',
                    width: '100%',
                  }}
                />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('Privacy');
                  }}>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Image source={require('../../assets/privacy.png')} />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      {text.policy}
                    </Text>
                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />
                <View
                  style={{
                    height: 1,
                    backgroundColor: 'lightslategrey',
                    width: '100%',
                  }}
                />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('Payment');
                  }}>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Image source={require('../../assets/terms.png')} />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      {text.paymentTermsHeader}
                    </Text>
                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />
                <View
                  style={{
                    height: 1,
                    backgroundColor: 'lightslategrey',
                    width: '100%',
                  }}
                />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('AboutUs');
                  }}>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Icon
                      name={'email-outline'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      {text.about}
                    </Text>
                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />
                <View
                  style={{
                    height: 1,
                    backgroundColor: 'lightslategrey',
                    width: '100%',
                  }}
                />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('AboutTicket');
                  }}>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Icon
                      name={'email-outline'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      {text.aboutTicketPartner}
                    </Text>
                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />
                <View
                  style={{
                    height: 1,
                    backgroundColor: 'lightslategrey',
                    width: '100%',
                  }}
                />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('Help');
                  }}>
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Image source={require('../../assets/information.png')} />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      {text.helpHeader}
                    </Text>
                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />
                <View
                  style={{
                    height: 1,
                    backgroundColor: 'lightslategrey',
                    width: '100%',
                  }}
                />
                <View style={{height: normalize(10)}} />
                <TouchableOpacity
                  onPress={() => {
                    this.logOut();
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignContent: 'center',
                      justifyContent: 'center',
                    }}>
                    <Icon
                      name={'logout'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                    <View style={{width: normalize(10)}} />
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(18),
                        fontWeight: 'bold',
                        flex: 1,
                      }}>
                      {text.signOut}
                    </Text>
                    <Icon
                      name={'chevron-right'}
                      type="material-community"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View style={{height: normalize(10)}} />
                <View
                  style={{
                    height: 1,
                    backgroundColor: 'lightslategrey',
                    width: '100%',
                  }}
                />
              </View>
            </ScrollView>
          </ImageBackground>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  bottomPortion: {
    padding: normalize(10),
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
  },
});
