import React, {Component} from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  FlatList,
  SafeAreaView,
} from 'react-native';
import normalize from 'react-native-normalize';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Color from '../constants/Color';
import {getSold} from '../screens/api';
import text from '../constants/strings';

export default class SaleHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {data: []};
  }
  componentDidMount() {
    this.getDetails();
  }

  getDetails = () => {
    AsyncStorage.getItem('session').then(async (value) => {
      if (value !== null) {
        const data = JSON.parse(value);
        const user = {
          KEY_NAMEVALUE: {
            KEY_NAME: 'PROCESS',
            KEY_VALUE: 'DISTRIBUTORSOLDLIST',
          },
          ADDITIONAL_INFO: JSON.stringify({DISTRIBUTORID: data.REF_ID}),
        };
        const res = await getSold(user);
        console.log(res);
        if (res.code === 200) {
          this.setState({
            data: res.json.RESPONSEINFO.DISTRIBUTORSOLDLIST,
          });
        }
      }
    });
  };
  renderItem = ({item}) => (
    <View style={styles.item}>
      <View
        style={{
          marginStart: normalize(8),
          backgroundColor: Color.BACKGROUND_COLOR,
          borderRadius: normalize(5),
          padding: normalize(8),
        }}>
        <View style={{flexDirection: 'row', flex: 1, justifyContent: 'center'}}>
          <Text style={styles.title}>{item.CUSTOMERMOBILENUMBER}</Text>
        </View>

        <View style={{flex: 1, flexDirection: 'row'}}>
          <View style={{flex: 1}}>
            <View style={{flexDirection: 'row'}}>
              <Text style={{fontSize: normalize(14), color: 'white'}}>
                {item.MOVIE}
              </Text>
            </View>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <Text style={{fontSize: normalize(12), color: 'white'}}>
                {text.ticketQuantity}
              </Text>
              <Text style={{fontSize: normalize(12), color: 'white'}}>
                {' ' + item.SOLDQUANTITY}
              </Text>
            </View>
          </View>
          <View>
            <View
              style={{
                flex: 1,
                position: 'absolute',
                bottom: 0,
                alignSelf: 'flex-end',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                }}>
                <Text
                  style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>
                  {text.sellDate}
                </Text>
                <Text
                  style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>
                  {item.SALE_DATE}
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                }}>
                <Text style={{fontSize: normalize(12), color: 'white'}}>
                  {text.orderId}
                </Text>
                <Text style={{fontSize: normalize(12), color: 'white'}}>
                  {item.ORDER_ID}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
  EmptyListMessage = () => {
    return this.state.data == null || this.state.data.length == 0 ? (
      <Text style={styles.emptyListStyle}>{text.noData}</Text>
    ) : null;
  };
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ImageBackground
          source={require('../../assets/login_bg.png')}
          style={styles.image}>
          <View style={{flex: 1}}>
            <FlatList
              data={this.state.data}
              renderItem={this.renderItem}
              keyExtractor={(item) => item.ORDER_ID}
              extraData={this.state.shouldRefresh}
              ListEmptyComponent={this.EmptyListMessage()}
            />
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  emptyListStyle: {
    padding: 10,
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },

  item: {
    backgroundColor: Color.APP_YELLOW,
    borderRadius: 5,

    marginVertical: 8,
    marginHorizontal: 8,
  },
  title: {
    flex: 1,
    fontSize: normalize(20),
    color: Color.APP_YELLOW,
  },
  subHeading: {
    fontSize: normalize(12),
    color: Color.ICON_COLOR,
  },
});
