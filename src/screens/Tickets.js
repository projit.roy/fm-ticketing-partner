import React, {Component} from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import normalize from 'react-native-normalize';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import Color from '../constants/Color';
import {getAssets} from '../screens/api';
import text from '../constants/strings';

export default class Tickets extends Component {
  constructor(props) {
    super(props);
    this.state = {data: [], loader: false};
  }
  componentDidMount() {
    this.getDetails();
  }

  getDetails = () => {
    this.setState({loader: true});
    AsyncStorage.getItem('session').then(async (value) => {
      if (value !== null) {
        const data = JSON.parse(value);
        const user = {
          KEY_NAMEVALUE: {
            KEY_NAME: 'PROCESS',
            KEY_VALUE: 'AVALIABLEASSESTS',
          },
          ADDITIONAL_INFO: JSON.stringify({DISTRIBUTORID: data.REF_ID}),
        };
        const res = await getAssets(user);
        console.log(res.json.RESPONSEINFO.AVALIABLEASSESTS);
        if (res.code === 200) {
          this.setState({loader: false});
          this.setState({
            data: res.json.RESPONSEINFO.AVALIABLEASSESTS,
          });
        }
      }
    });
  };
  renderItem = ({item}) => (
    <TouchableOpacity
      style={styles.item}
      onPress={() => {
        this.props.navigation.navigate('ReviewBuyOne', {item: item});
      }}>
      <View
        style={{
          height: '100%',
          marginStart: normalize(8),
          backgroundColor: Color.BACKGROUND_COLOR,
          borderRadius: normalize(5),
          padding: normalize(8),
        }}>
        <Text style={styles.title}>{item.MOVIE}</Text>
        <Text style={styles.subHeading}>{item.VALIDITY}</Text>
      </View>
    </TouchableOpacity>
  );
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Spinner visible={this.state.loader} textContent="" />
        <ImageBackground
          source={require('../../assets/login_bg.png')}
          style={styles.image}>
          <View style={{flex: 1, margin: normalize(5), alignSelf: 'center'}}>
            <Text
              style={{
                color: 'white',
                alignSelf: 'center',
                fontWeight: 'bold',
                fontSize: normalize(20),
                margin: normalize(8),
              }}>
              {text.buyMovie}
            </Text>
            <FlatList
              data={this.state.data}
              renderItem={this.renderItem}
              keyExtractor={(item) => item.id}
              numColumns={2}
            />
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  checkBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  upperPortion: {
    margin: normalize(10),
  },
  bottomPortion: {
    margin: normalize(10),

    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: normalize(20),
    textAlign: 'center',
    margin: normalize(10),
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },

  item: {
    width: '45%',
    backgroundColor: Color.APP_YELLOW,
    borderRadius: normalize(5),
    marginTop: 5,
    marginBottom: 5,
    marginVertical: normalize(4),
    marginHorizontal: normalize(8),
  },
  title: {
    fontSize: normalize(18),
    color: 'white',
  },
  subHeading: {
    fontSize: normalize(12),
    color: Color.ICON_COLOR,
  },
});
