import React, {Component} from 'react';
import {ScrollView, TouchableOpacity, Platform, StatusBar} from 'react-native';
import HTML from 'react-native-render-html';
import Icon from 'react-native-easy-icon';
import text from '../constants/strings';

export default class TermsCondition extends Component {
  render() {
    return (
      <ScrollView
        style={{
          flex: 1,
          paddingRight: 10,
          paddingLeft: 10,
          backgroundColor: '#011233',
        }}>
        <TouchableOpacity
          style={{
            paddingStart: 2,
            paddingTop: 15,
            marginTop: Platform.OS === 'ios' ? 20 : 0,
          }}
          onPress={() => this.props.navigation.goBack()}>
          <Icon
            name={'arrow-left'}
            type="material-community"
            size={24}
            color="white"
          />
        </TouchableOpacity>
        <HTML source={{html: text.privacyPolicy}} contentWidth={100} />
      </ScrollView>
    );
  }
}
