import React, {Component} from 'react';
import {
  ScrollView,
  Image,
  TouchableOpacity,
  ImageBackground,
  Text,
  Platform,
} from 'react-native';
import Icon from 'react-native-easy-icon';
import text from '../constants/strings';

export default class TermsCondition extends Component {
  render() {
    return (
      <ScrollView style={{flex: 1}}>
        <ImageBackground
          source={require('../../assets/login_bg.png')}
          style={{
            flex: 1,
            //justifyContent: 'center',
            resizeMode: 'cover',
          }}>
          <TouchableOpacity
            style={{
              padding: 15,
              flexDirection: 'row',
              marginTop: Platform.OS === 'ios' ? 20 : 0,
            }}
            onPress={() => this.props.navigation.goBack()}>
            <Icon
              name={'arrow-left'}
              type="material-community"
              size={24}
              color="white"
            />
            <Text style={{color: 'white', fontSize: 18, marginStart: 6}}>
              {text.aboutTicketPartner}
            </Text>
          </TouchableOpacity>
          <Image
            style={{width: '100%', height: 500, resizeMode: 'contain'}}
            source={require('../../assets/index.jpeg')}
          />
          <Image
            style={{
              width: '100%',
              height: 500,
              resizeMode: 'contain',
              marginTop: 5,
            }}
            source={require('../../assets/index2.jpeg')}
          />
        </ImageBackground>
      </ScrollView>
    );
  }
}
