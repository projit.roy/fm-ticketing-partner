import React, {Component} from 'react';
import {
  ScrollView,
  View,
  ImageBackground,
  TouchableOpacity,
  Text,
  TextInput,
  StyleSheet,
  FlatList,
  Platform,
  Image,
} from 'react-native';
import Icon from 'react-native-easy-icon';
import Color from '../../constants/Color';
import Snackbar from 'react-native-snackbar';
import Spinner from 'react-native-loading-spinner-overlay';
import Menu, {MenuItem} from 'react-native-material-menu';
import {generateOtp, createUser, checkUser, getCity} from '../api';
import text from '../../constants/strings';

export default class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userName: '',
      phoneNumber: '',
      password: '',
      confirm: '',
      isTermsChecked: false,
      address: '',
      city: '',
      pincode: '',
      tradeName: '',
      aadhhar: '',
      gst: '',
      loader: false,
      data: [],
      query: '',
      selectedItem: {},
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.route.params?.otp !== this.props.route.params?.otp) {
      const result = this.props.route.params?.otp;
      if (result === 'verified') {
        this.createDistributor();
      }
    }
  }

  // set menu list
  setMenuRef = (ref) => {
    this.menu = ref;
  };

  // hide menu items
  hideMenu = () => {
    this.menu.hide();
  };

  // show menu items
  showMenu = () => {
    this.menu.show();
  };

  createDistributor = async () => {
    const val = Math.floor(10000 + Math.random() * 90000);
    console.log(val);
    const data = {
      OPERATIONALENTITYINFO: {
        ENTITYCODE: val.toString(),
        ENTITYTYPE: 'DISTRIBUTO',
        ENTITYNAME: this.state.userName,
        BOOKORDERS: 'Y',
        RECEIVEPAYMENTS: 'Y',
        HOLDINVENTORY: 'Y',
        RESELLHARDWARE: '',
        RESPFORCUSTOMERDUES: 'Y',
        STATUS: '',
        NOOFCONCURRENTUSERS: '3',
        PARENTENTITYID: '539',
        PORTALACCESSREQD: 'Y',
        USERNAME: this.state.phoneNumber,
        PASSWORD: this.state.password,
        MOBILEPHONE: '91' + this.state.phoneNumber,
        CURRENCYCODE: 'INR',
        NATIONALITY: 'INDIAN',
        UINTYPE: '',
        UIN: '',
        RMNVERIFIED: 'Y',
        ADDRESSINFO: {
          ADDRESSTYPECODE: 'PRI',
          ADDRESS1: this.state.address,
          CITY: this.state.city,
          DISTRICT: '',
          ZIPCODE: this.state.pincode,
          STATE: 'India',
          STATECODE: 'IND',
          COUNTRY: 'INDIA',
          COUNTRYCODE: 'IND',
          LOCATION: '',
        },
        FLEXATTRIBUTEINFO: {
          ATTRIBUTE1: this.state.tradeName,
          ATTRIBUTE2: this.state.aadhhar,
          ATTRIBUTE3: this.state.gst,
        },
      },
    };
    console.log(data);
    this.setState({loader: true});
    const res = await createUser(data);
    console.log(res);
    if (res.code === 200) {
      let json = res.json.RESPONSEINFO;
      if (json.STATUS.ERRORNO === '0') {
        this.setState({loader: false});
        setTimeout(
          () =>
            Snackbar.show({
              text: text.successUser,
              duration: Snackbar.LENGTH_SHORT,
            }),
          500,
        );
        this.props.navigation.replace('Login');
      } else {
        this.setState({loader: false});
        setTimeout(
          () =>
            Snackbar.show({
              text: text.somethingWrong,
              duration: Snackbar.LENGTH_SHORT,
            }),
          500,
        );
      }
    }
  };

  validateFields() {
    if (this.state.userName === '') {
      Snackbar.show({
        text: text.userError1,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }
    if (this.state.userName.length < 3) {
      Snackbar.show({
        text: text.userError2,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }
    if (this.state.phoneNumber === '') {
      Snackbar.show({
        text: text.phonError1,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }
    if (this.state.phoneNumber.length !== 10) {
      Snackbar.show({
        text: text.phoneError2,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }
    if (this.state.password === '') {
      Snackbar.show({
        text: text.passwordError1,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }
    if (this.state.password.length < 7) {
      Snackbar.show({
        text: text.passwordError2,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }
    if (this.state.password !== this.state.confirm) {
      Snackbar.show({
        text: text.passwordError3,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }
    if (this.state.address === '') {
      Snackbar.show({
        text: text.addressError,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }
    if (this.state.city === '') {
      Snackbar.show({
        text: text.cityError,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }
    if (this.state.pincode === '') {
      Snackbar.show({
        text: text.cityError,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }
    if (this.state.aadhhar !== '') {
      if (this.state.aadhhar.length !== 10) {
        Snackbar.show({
          text: text.panError,
          duration: Snackbar.LENGTH_SHORT,
        });
        return false;
      }
    }
    if (this.state.gst !== '') {
      if (this.state.gst.length !== 15) {
        Snackbar.show({
          text: text.gstError,
          duration: Snackbar.LENGTH_SHORT,
        });
        return false;
      }
    }
    if (!this.state.isTermsChecked) {
      Snackbar.show({
        text: text.termsError,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }
    return true;
  }

  check = async () => {
    if (this.validateFields()) {
      this.setState({loader: true});
      //this.getOtp();
      let data = {CHECKUSERAVAILABILITY: {USERNAME: this.state.phoneNumber}};
      console.log(data);
      const res = await checkUser(data);
      console.log(res);
      if (res.code === 200) {
        let json = res.json.RESPONSEINFO;
        console.log(res);
        if (json.STATUS.ERRORNO === '0') {
          console.log(json.ISUSERAVAILABLE);
          if (json.ISUSERAVAILABLE !== 'FALSE') {
            this.registerUser();
          } else {
            this.setState({loader: false});
            setTimeout(
              () =>
                Snackbar.show({
                  text: text.userAvailable,
                  duration: Snackbar.LENGTH_SHORT,
                }),
              500,
            );
          }
        } else if (json.STATUS.ERRORNO === '110521') {
          this.setState({loader: false});
          Snackbar.show({
            text: text.code110521,
            duration: Snackbar.LENGTH_SHORT,
          });
        } else {
          this.setState({loader: false});
          Snackbar.show({
            text: text.somethingWrong,
            duration: Snackbar.LENGTH_SHORT,
          });
        }
      } else {
        this.setState({loader: false});
        Snackbar.show({
          text: text.somethingWrong,
          duration: Snackbar.LENGTH_SHORT,
        });
      }
    }
  };

  async registerUser() {
    if (this.validateFields()) {
      this.setState({isRegistering: true});
      let data = {
        GENERATEOTP: {
          MOBILEPHONE: this.state.phoneNumber,
          OTPEMAIL: '',
          PARTYID: '0',
          COUNTRYCODE: '',
          RESEND: 'FALSE',
          ATTRIBUTE1: '',
        },
      };
      const res = await generateOtp(data);
      if (res.code === 200) {
        let json = res.json.RESPONSEINFO;
        if (json.STATUS.ERRORNO === '0') {
          this.setState({loader: false});
          this.props.navigation.navigate('VerifyOTP', {
            params: {
              phoneNumber: this.state.phoneNumber,
              screen: 'Register',
            },
          });
        } else if (json.STATUS.ERRORNO === '110521') {
          this.setState({loader: false});
          Snackbar.show({
            text: text.code110521,
            duration: Snackbar.LENGTH_SHORT,
          });
        } else {
          this.setState({loader: false});
          Snackbar.show({
            text: text.somethingWrong,
            duration: Snackbar.LENGTH_SHORT,
          });
        }
      }
    }
  }

  cityList = (texts) => {
    //console.log(text);
    this.setState({city: texts});
    if (texts.length > 2) {
      getCity(texts.toString()).then((res) => {
        //console.log({res});
        if (res.code === 200) {
          let data = res.json;
          //console.log(data);
          let list = [];
          for (let i = 0; i < data.data.length; i++) {
            const obj = data.data[i];
            list.push(obj.city);
          }
          //console.log(list);
          this.showMenu();
          this.setState({data: list});
        }
      });
    }
  };

  render() {
    return (
      <ScrollView contentContainerStyle={styles.container} bounces={false}>
        <Spinner visible={this.state.loader} textContent="" />
        <View style={styles.container}>
          <ImageBackground
            source={require('../../../assets/login_bg.png')}
            style={styles.image}>
            <TouchableOpacity
              style={{padding: 8, marginTop: Platform.OS === 'ios' ? 20 : 25}}
              onPress={() => this.props.navigation.goBack()}>
              <Icon
                name={'arrow-left'}
                type="material-community"
                size={24}
                color="white"
              />
            </TouchableOpacity>
            <View
              style={{
                justifyContent: 'center',
                flex: 1,
                alignItems: 'center',
              }}>
              <Image
                style={{width: 100, height: 100, marginBottom: 10}}
                source={require('../../../assets/ICON.png')}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                height: 50,
                backgroundColor: Color.BACKGROUND_COLOR,
                borderRadius: 25,
              }}>
              <Icon
                style={{alignSelf: 'center', margin: 10}}
                name={'account'}
                type="material-community"
                size={20}
                color={Color.ICON_COLOR}
              />
              <TextInput
                style={{flex: 1, color: 'white'}}
                placeholder={text.placeholderName + '*'}
                onChangeText={(text) => {
                  this.setState({userName: text});
                }}
                placeholderTextColor={Color.ICON_COLOR}
                value={this.state.userName}
              />
            </View>
            <View style={styles.inputContainer}>
              <Icon
                style={{alignSelf: 'center', margin: 10}}
                name={'phone'}
                type="material-community"
                size={20}
                color={Color.ICON_COLOR}
              />
              <TextInput
                style={{flex: 1, color: 'white'}}
                placeholder={text.placeholderPhone + '*'}
                keyboardType="phone-pad"
                onChangeText={(text) => this.setState({phoneNumber: text})}
                placeholderTextColor={Color.ICON_COLOR}
                value={this.state.phoneNumber}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                height: 50,
                backgroundColor: Color.BACKGROUND_COLOR,
                borderRadius: 25,
                marginTop: 5,
              }}>
              <Icon
                style={{alignSelf: 'center', margin: 10}}
                name={'lock-outline'}
                type="material-community"
                size={20}
                color={Color.ICON_COLOR}
              />
              <TextInput
                style={{flex: 1, color: 'white'}}
                onChangeText={(text) => this.setState({password: text})}
                placeholder={text.placeholderPassword + '*'}
                secureTextEntry={true}
                placeholderTextColor={Color.ICON_COLOR}
                value={this.state.password}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                height: 50,
                backgroundColor: Color.BACKGROUND_COLOR,
                borderRadius: 25,
                marginTop: 5,
              }}>
              <Icon
                style={{alignSelf: 'center', margin: 10}}
                name={'lock-outline'}
                type="material-community"
                size={20}
                color={Color.ICON_COLOR}
              />
              <TextInput
                style={{flex: 1, color: 'white'}}
                onChangeText={(text) => this.setState({confirm: text})}
                placeholder={text.placeholderConfirmPassword + '*'}
                secureTextEntry={true}
                placeholderTextColor={Color.ICON_COLOR}
                value={this.state.confirm}
              />
            </View>
            <View style={styles.inputContainer}>
              <Icon
                style={{alignSelf: 'center', margin: 10}}
                name={'map-marker'}
                type="material-community"
                size={20}
                color={Color.ICON_COLOR}
              />
              <TextInput
                style={{flex: 1, color: 'white'}}
                placeholder={text.placeholderAddress + '*'}
                onChangeText={(text) => this.setState({address: text})}
                placeholderTextColor={Color.ICON_COLOR}
                value={this.state.address}
              />
            </View>
            <View style={styles.inputContainer}>
              <Icon
                style={{alignSelf: 'center', margin: 10}}
                name={'map-marker'}
                type="material-community"
                size={20}
                color={Color.ICON_COLOR}
              />
              <TextInput
                style={{flex: 1, color: 'white'}}
                placeholder={text.placeholderCity + '*'}
                placeholderTextColor={Color.ICON_COLOR}
                onChangeText={(text) => this.cityList(text)}
                value={this.state.city}
              />
              <Menu ref={this.setMenuRef}>
                <FlatList
                  data={this.state.data}
                  renderItem={({item}) => (
                    <MenuItem
                      style={{backgroundColor: Color.BACKGROUND_COLOR}}
                      onPress={() => {
                        this.setState({city: item});
                        this.hideMenu();
                      }}>
                      <Text style={{color: 'white'}}>{item}</Text>
                    </MenuItem>
                  )}
                  keyExtractor={(item) => item.id}
                />
              </Menu>
            </View>
            <View style={styles.inputContainer}>
              <Icon
                style={{alignSelf: 'center', margin: 10}}
                name={'map-marker'}
                type="material-community"
                size={20}
                color={Color.ICON_COLOR}
              />
              <TextInput
                style={{flex: 1, color: 'white'}}
                onChangeText={(text) => this.setState({pincode: text})}
                placeholder={text.placeholderPincode + '*'}
                keyboardType="number-pad"
                maxLength={6}
                placeholderTextColor={Color.ICON_COLOR}
                value={this.state.pincode}
              />
            </View>
            <View style={styles.inputContainer}>
              <Icon
                style={{alignSelf: 'center', margin: 10}}
                name={'map-marker'}
                type="material-community"
                size={20}
                color={Color.ICON_COLOR}
              />
              <TextInput
                style={{flex: 1, color: 'white'}}
                onChangeText={(text) => this.setState({pincode: text})}
                placeholder="Pincode"
                keyboardType="number-pad"
                maxLength={6}
                placeholderTextColor={Color.ICON_COLOR}
                value={this.state.pincode}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                height: 50,
                backgroundColor: Color.BACKGROUND_COLOR,
                borderRadius: 25,
                marginTop: 5,
              }}>
              <Icon
                style={{alignSelf: 'center', margin: 10}}
                name={'account-box'}
                type="material-community"
                size={20}
                color={Color.ICON_COLOR}
              />
              <TextInput
                style={{flex: 1, color: 'white'}}
                autoCapitalize="characters"
                placeholder="Trade Name(Optional)"
                onChangeText={(text) => this.setState({tradeName: text})}
                placeholderTextColor={Color.ICON_COLOR}
                value={this.state.tradeName}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                height: 50,
                backgroundColor: Color.BACKGROUND_COLOR,
                borderRadius: 25,
                marginTop: 5,
              }}>
              <Icon
                style={{alignSelf: 'center', margin: 10}}
                name={'account-box'}
                type="material-community"
                size={20}
                color={Color.ICON_COLOR}
              />
              <TextInput
                autoCapitalize="characters"
                style={{flex: 1, color: 'white'}}
                placeholder="Pan Number(Optional)"
                onChangeText={(text) => this.setState({aadhhar: text})}
                placeholderTextColor={Color.ICON_COLOR}
                value={this.state.aadhhar}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                height: 50,
                backgroundColor: Color.BACKGROUND_COLOR,
                borderRadius: 25,
                marginTop: 5,
              }}>
              <Icon
                style={{alignSelf: 'center', margin: 10}}
                name={'account-box'}
                type="material-community"
                size={20}
                color={Color.ICON_COLOR}
              />
              <TextInput
                style={{flex: 1, color: 'white'}}
                placeholder="GST Number(Optional)"
                autoCapitalize="characters"
                onChangeText={(text) => this.setState({gst: text})}
                placeholderTextColor={Color.ICON_COLOR}
                value={this.state.gst}
              />
            </View>
            <View style={{height: 30}} />
            <TouchableOpacity
              disabled={this.state.isRegistering}
              // onPress={() => navigationRef.current?.navigate('VerifyOTP')}
              onPress={() => {
                this.check();
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  height: 50,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: Color.BACKGROUND_COLOR,
                  borderRadius: 25,
                }}>
                <Text
                  accessibilityState={{disabled: this.state.isRegistering}}
                  style={{
                    color: 'white',
                    fontSize: 16,
                    alignSelf: 'center',
                  }}>
                  {text.signUp}
                </Text>
              </View>
            </TouchableOpacity>
            <View
              style={{flexDirection: 'row', marginTop: 10, marginBottom: 10}}>
              <TouchableOpacity
                style={styles.checkBox}
                onPress={() => {
                  this.setState({
                    isTermsChecked: !this.state.isTermsChecked,
                  });
                }}>
                <Icon
                  size={20}
                  color={'white'}
                  type="material-community"
                  name={
                    this.state.isTermsChecked
                      ? 'checkbox-marked-outline'
                      : 'checkbox-blank-outline'
                  }
                />
              </TouchableOpacity>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text
                  style={{
                    color: Color.ICON_COLOR,
                    fontSize: 12,
                  }}>
                  {' '}
                  {text.aggree}
                </Text>
                <Text
                  style={{
                    color: 'white',
                    fontWeight: 'bold',
                    fontSize: 12,
                  }}>
                  {' '}
                  {text.terms}
                </Text>
                <Text
                  style={{
                    color: Color.ICON_COLOR,
                    fontSize: 12,
                  }}>
                  {' '}
                  {text.and}{' '}
                </Text>
                <Text
                  style={{
                    color: 'white',
                    fontWeight: 'bold',
                    fontSize: 12,
                  }}>
                  {' '}
                  {text.policy}
                </Text>
              </View>
            </View>
          </ImageBackground>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    //paddingBottom: 20,
  },
  checkBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  upperPortion: {
    margin: 10,
  },
  bottomPortion: {
    margin: 10,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    padding: 8,
  },
  inputContainer: {
    marginTop: 5,
    flexDirection: 'row',
    height: 50,
    backgroundColor: Color.BACKGROUND_COLOR,
    borderRadius: 25,
  },
});
