import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  Platform,
} from 'react-native';
import Icon from 'react-native-easy-icon';
import text from '../constants/strings';

export default class TermsCondition extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <ImageBackground
          source={require('../../assets/login_bg.png')}
          style={{
            flex: 1,
            //justifyContent: 'center',
            resizeMode: 'cover',
          }}>
          <TouchableOpacity
            style={{padding: 15, marginTop: Platform.OS === 'ios' ? 20 : 0}}
            onPress={() => this.props.navigation.goBack()}>
            <Icon
              name={'arrow-left'}
              type="material-community"
              size={24}
              color="white"
            />
          </TouchableOpacity>
          <Text
            style={{
              flexWrap: 'wrap',
              marginRight: 10,
              marginLeft: 10,
              fontSize: 18,
              color: 'white',
            }}>
            {text.paymentTerms}
          </Text>
        </ImageBackground>
      </View>
    );
  }
}
