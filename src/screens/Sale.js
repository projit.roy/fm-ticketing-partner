import React, {Component} from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import normalize from 'react-native-normalize';
import Color from '../constants/Color';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import Snackbar from 'react-native-snackbar';
import {getSales} from '../screens/api';
import text from '../constants/strings';

export default class Sale extends Component {
  constructor(props) {
    super(props);
    this.state = {data: [], loader: false};
  }
  componentDidMount() {
    this.getDetails();
    this.props.navigation.addListener('willFocus', () => {
      this.getDetails();
    });
  }

  getDetails = () => {
    this.setState({loader: true});
    AsyncStorage.getItem('session').then(async (value) => {
      if (value !== null) {
        const data = JSON.parse(value);
        const user = {
          KEY_NAMEVALUE: {
            KEY_NAME: 'PROCESS',
            KEY_VALUE: 'DISTRIBUTORSALEMOVIES',
          },
          ADDITIONAL_INFO: JSON.stringify({DISTRIBUTORID: data.REF_ID}),
        };
        const res = await getSales(user);
        console.log(res);
        console.log(
          res.json.RESPONSEINFO.DISTRIBUTORSALEMOVIES
            ? [res.json.RESPONSEINFO.DISTRIBUTORSALEMOVIES]
            : [],
        );
        if (res.code === 200) {
          this.setState({
            data: res.json.RESPONSEINFO.DISTRIBUTORSALEMOVIES
              ? res.json.RESPONSEINFO.DISTRIBUTORSALEMOVIES
              : [],
            loader: false,
          });
        }
      } else {
        this.setState({loader: false});
      }
    });
  };

  renderItem = ({item}) => (
    <View style={styles.item}>
      <TouchableOpacity
        onPress={() => {
          if (Number(item.BALANCE_TICKETS) > 0) {
            this.props.navigation.navigate('ReviewSaleOne', {
              movieName: item.MOVIE,
              movieGenere: 'Action',
              expiryDate: item.VALIDITY_TO,
              balance: item.BALANCE_TICKETS,
              price: item.PRICE,
              movieCode: item.MOVIECODE,
            });
          } else {
            Snackbar.show({
              text: text.noTicket,
              duration: Snackbar.LENGTH_SHORT,
            });
          }
        }}>
        <View
          style={{
            marginStart: normalize(8),
            backgroundColor: Color.BACKGROUND_COLOR,
            borderRadius: normalize(5),
            padding: normalize(8),
          }}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 1.3}}>
              <View style={{flex: 1}}>
                <Text style={styles.title}>{item.MOVIE}</Text>
                <Text style={styles.subHeading}>{item.VALIDITY_TO}</Text>
              </View>
            </View>
            <View style={{flex: 1}}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                }}>
                <Text style={{fontSize: normalize(14), color: 'white'}}>
                  {text.ticketPrice}:{' '}
                </Text>
                <Text style={{fontSize: normalize(14), color: 'white'}}>
                  {item.PRICE}
                </Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                }}>
                <Text style={{fontSize: normalize(14), color: 'white'}}>
                  {text.balance} :
                </Text>
                <Text
                  style={{
                    fontSize: normalize(14),
                    color: 'green',
                    marginEnd: 3,
                  }}>
                  {item.BALANCE_TICKETS}
                </Text>
                <Text style={{fontSize: normalize(14), color: 'white'}}>
                  {text.ticket}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Spinner visible={this.state.loader} textContent="" />
        <ImageBackground
          source={require('../../assets/login_bg.png')}
          style={styles.image}>
          <View style={{flex: 1}}>
            <FlatList
              data={this.state.data}
              renderItem={this.renderItem}
              keyExtractor={(item) => item.id}
            />
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },

  item: {
    backgroundColor: Color.APP_YELLOW,
    borderRadius: normalize(5),

    marginVertical: normalize(8),
    marginHorizontal: normalize(8),
  },
  title: {
    fontSize: normalize(20),
    color: 'white',
  },
  subHeading: {
    fontSize: normalize(14),
    color: Color.ICON_COLOR,
  },
});
