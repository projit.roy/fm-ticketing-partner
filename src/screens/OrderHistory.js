import React, {Component} from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  FlatList,
  SafeAreaView,
} from 'react-native';
import normalize from 'react-native-normalize';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Color from '../constants/Color';
import text from '../constants/strings';
import {getOrders} from '../screens/api';

export default class OrderHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {data: []};
  }
  componentDidMount() {
    this.getDetails();
  }

  getDetails = () => {
    AsyncStorage.getItem('session').then(async (value) => {
      if (value !== null) {
        const data = JSON.parse(value);
        const user = {
          KEY_NAMEVALUE: {
            KEY_NAME: 'PROCESS',
            KEY_VALUE: 'DISTRIBUTORPURCHASELIST',
          },
          ADDITIONAL_INFO: JSON.stringify({DISTRIBUTORID: data.REF_ID}),
        };
        const res = await getOrders(user);
        if (res.code === 200) {
          this.setState({
            data: res.json.RESPONSEINFO.DISTRIBUTORPURCHASELIST,
          });
        }
      }
    });
  };

  renderItem = ({item}) => (
    <View style={styles.item}>
      <View
        style={{
          marginStart: normalize(8),
          backgroundColor: Color.BACKGROUND_COLOR,
          borderRadius: normalize(5),
          padding: normalize(8),
        }}>
        <View style={{flexDirection: 'row', flex: 1, justifyContent: 'center'}}>
          <Text style={styles.title}>{item.MOVIE}</Text>
        </View>
        <Text style={styles.subHeading}>({item.LANGUAGE})</Text>

        <View style={{flex: 1, flexDirection: 'row'}}>
          <View style={{flex: 1.3}}>
            <View style={{flexDirection: 'row'}}>
              <Text style={{fontSize: normalize(14), color: 'white'}}>
                {text.purchased}
                {item.PURCHASEDQUANTITY} {text.ticket},
              </Text>
              <Text
                style={{
                  fontSize: normalize(14),
                  marginStart: normalize(5),
                  color: 'white',
                }}>
                {item.POSTER_QUANTITY} {text.poster}
              </Text>
            </View>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>
                {text.purchasedDate}
              </Text>
              <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>
                {item.PURCHASED_DATE}
              </Text>
            </View>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>
                {text.expiryDate}
              </Text>
              <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>
                {item.MOVIE_EXPIRYDATE}
              </Text>
            </View>
          </View>
          <View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                position: 'absolute',
                bottom: 0,
                alignSelf: 'flex-end',
              }}>
              <Text style={{fontSize: normalize(12), color: 'white'}}>
                {text.orderId}
              </Text>
              <Text
                style={{fontSize: normalize(12), color: 'white', marginEnd: 3}}>
                {item.ORDER_ID}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ImageBackground
          source={require('../../assets/login_bg.png')}
          style={styles.image}>
          <View style={{flex: 1}}>
            <FlatList
              data={this.state.data}
              renderItem={this.renderItem}
              keyExtractor={(item) => item.id}
            />
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },

  item: {
    backgroundColor: Color.APP_YELLOW,
    borderRadius: 5,

    marginVertical: 8,
    marginHorizontal: 8,
  },
  title: {
    flex: 1,
    fontSize: normalize(20),
    color: Color.APP_YELLOW,
  },
  subHeading: {
    fontSize: normalize(12),
    color: Color.ICON_COLOR,
  },
});
