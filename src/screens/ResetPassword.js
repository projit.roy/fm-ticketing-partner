import React, {Component} from 'react';
import {
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Platform,
} from 'react-native';
import Icon from 'react-native-easy-icon';
import Color from '../constants/Color';
import Snackbar from 'react-native-snackbar';
import Spinner from 'react-native-loading-spinner-overlay';
import {
  generateOtp,
  checkUser,
  getDistributorDetails,
  forgotPassword,
} from '../screens/api';
import text from '../constants/strings';

export default class ResetPassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      phoneNumber: '',
      password: '',
      passwordConfirm: '',
      verified: false,
      params: props,
      loader: false,
    };
  }
  componentDidMount() {}

  componentDidUpdate(prevProps) {
    if (prevProps.route.params?.otp !== this.props.route.params?.otp) {
      const result = this.props.route.params?.otp;
      if (result === 'verified') {
        this.getDistributor();
      }
    }
  }

  getDistributor = async () => {
    this.setState({loader: true});
    const user = {
      KEY_NAMEVALUE: {
        KEY_NAME: 'PROCESS',
        KEY_VALUE: 'DISTRIBUTORDETAILS',
      },
      ADDITIONAL_INFO: JSON.stringify({USERNAME: this.state.phoneNumber}),
    };
    const res = await getDistributorDetails(user);
    if (res.code === 200) {
      let json = res.json.RESPONSEINFO;
      if (json.STATUS.ERRORNO === '0') {
        this.changePassword(json.DISTRIBUTORDETAILS.USER_ID);
      }
    }
  };

  changePassword = async (id) => {
    const data = {
      SELFCARERESETPASSWORD: {
        USERID: id,
        NEWPASSWORD: this.state.password,
        CONFIRMPASSWORD: this.state.passwordConfirm,
      },
    };
    const res = await forgotPassword(data);
    console.log(res);
    if (res.code === 200) {
      let json = res.json.RESPONSEINFO;
      if (json.STATUS.ERRORNO === '0') {
        this.setState({loader: false});
        Snackbar.show({
          text: text.passwordChangedSuccess,
          duration: Snackbar.LENGTH_SHORT,
        });
        this.props.navigation.replace('Login');
      } else if (json.STATUS.ERRORNO === ' 40030') {
        this.setState({loader: false});
        Snackbar.show({
          text: text.code40030,
          duration: Snackbar.LENGTH_SHORT,
        });
      } else {
        this.setState({loader: false});
        Snackbar.show({
          text: 'Something went wrong!!Please try again',
          duration: Snackbar.LENGTH_SHORT,
        });
      }
    }
  };

  reset = async () => {
    if (this.validFields()) {
      this.setState({loader: true});
      //this.getOtp();
      let data = {CHECKUSERAVAILABILITY: {USERNAME: this.state.phoneNumber}};
      console.log(data);
      const res = await checkUser(data);
      console.log(res);
      if (res.code === 200) {
        let json = res.json.RESPONSEINFO;
        console.log(res);
        if (json.STATUS.ERRORNO === '0') {
          console.log(json.ISUSERAVAILABLE);
          if (json.ISUSERAVAILABLE !== 'TRUE') {
            this.getOtp();
          } else {
            this.setState({loader: false});
            setTimeout(
              () =>
                Snackbar.show({
                  text: text.userNotAvailable,
                  duration: Snackbar.LENGTH_SHORT,
                }),
              500,
            );
          }
        } else if (json.STATUS.ERRORNO === '110521') {
          this.setState({loader: false});
          Snackbar.show({
            text: text.code110521,
            duration: Snackbar.LENGTH_SHORT,
          });
        } else {
          this.setState({loader: false});
          Snackbar.show({
            text: text.somethingWrong,
            duration: Snackbar.LENGTH_SHORT,
          });
        }
      } else {
        this.setState({loader: false});
        Snackbar.show({
          text: text.somethingWrong,
          duration: Snackbar.LENGTH_SHORT,
        });
      }
    }
  };

  getOtp = async () => {
    let data = {
      GENERATEOTP: {
        MOBILEPHONE: this.state.phoneNumber,
        OTPEMAIL: '',
        PARTYID: '0',
        COUNTRYCODE: '',
        RESEND: 'FALSE',
        ATTRIBUTE1: '',
      },
    };
    const res = await generateOtp(data);
    if (res.code === 200) {
      this.setState({loader: false});
      let json = res.json.RESPONSEINFO;
      if (json.STATUS.ERRORNO === '0') {
        this.props.navigation.navigate('VerifyOTP', {
          params: {
            phoneNumber: this.state.phoneNumber,
            screen: 'ResetPassword',
          },
        });
      } else if (json.STATUS.ERRORNO === '110521') {
        Snackbar.show({
          text: text.code110521,
          duration: Snackbar.LENGTH_SHORT,
        });
      } else {
        Snackbar.show({
          text: text.somethingWrong,
          duration: Snackbar.LENGTH_SHORT,
        });
      }
    }
  };

  validFields = () => {
    if (this.state.phoneNumber === '') {
      Snackbar.show({
        text: text.phonError1,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }
    if (this.state.phoneNumber.length !== 10) {
      Snackbar.show({
        text: text.phoneError2,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }
    if (this.state.password === '') {
      Snackbar.show({
        text: text.passwordError1,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }
    if (this.state.passwordConfirm === '') {
      Snackbar.show({
        text: text.passwordError4,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }
    if (this.state.password.length < 7) {
      Snackbar.show({
        text: text.passwordError2,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }
    if (this.state.password !== this.state.passwordConfirm) {
      Snackbar.show({
        text: text.passwordError3,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }

    return true;
  };

  render() {
    return (
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <Spinner visible={this.state.loader} textContent="" />
        <View style={styles.container}>
          <ImageBackground
            source={require('../../assets/login_bg.png')}
            style={styles.image}>
            <View style={styles.upperPortion}>
              <TouchableOpacity
                style={{padding: 8, marginTop: Platform.OS === 'ios' ? 20 : 20}}
                onPress={() => this.props.navigation.goBack()}>
                <Icon
                  name={'arrow-left'}
                  type="material-community"
                  size={24}
                  color="white"
                />
              </TouchableOpacity>
              <View
                style={{
                  justifyContent: 'center',
                  flex: 1,
                  alignItems: 'center',
                }}>
                <Image
                  style={{width: 150, height: 150, marginBottom: 10}}
                  source={require('../../assets/ICON.png')}
                />
              </View>
              <Text style={{color: 'white', alignSelf: 'center'}}>
                {text.resetPass}
              </Text>
            </View>
            <View style={styles.bottomPortion}>
              <View
                style={{
                  flexDirection: 'row',
                  height: 50,
                  backgroundColor: Color.BACKGROUND_COLOR,
                  borderRadius: 50 / 2,
                  marginBottom: 10,
                }}>
                <Icon
                  style={{alignSelf: 'center', margin: 10}}
                  name={'phone'}
                  type="material-community"
                  size={20}
                  color={Color.ICON_COLOR}
                />
                <TextInput
                  style={{flex: 1, color: 'white'}}
                  placeholder={text.placeholderPhone}
                  onChangeText={(text) => {
                    this.setState({phoneNumber: text});
                  }}
                  value={this.state.phoneNumber}
                  placeholderTextColor={Color.ICON_COLOR}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  height: 50,
                  backgroundColor: Color.BACKGROUND_COLOR,
                  borderRadius: 50 / 2,
                }}>
                <Icon
                  style={{alignSelf: 'center', margin: 10}}
                  name={'lock-outline'}
                  type="material-community"
                  size={20}
                  color={Color.ICON_COLOR}
                />
                <TextInput
                  style={{flex: 1, color: 'white'}}
                  placeholder={text.placeholderPassword}
                  placeholderTextColor={Color.ICON_COLOR}
                  onChangeText={(text) => {
                    this.setState({password: text});
                  }}
                  value={this.state.password}
                  secureTextEntry={true}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  height: 50,
                  backgroundColor: Color.BACKGROUND_COLOR,
                  borderRadius: 50 / 2,
                  marginTop: 15,
                }}>
                <Icon
                  style={{alignSelf: 'center', margin: 10}}
                  name={'lock-outline'}
                  type="material-community"
                  size={20}
                  color={Color.ICON_COLOR}
                />
                <TextInput
                  style={{flex: 1, color: 'white'}}
                  placeholder={text.placeholderConfirmPassword}
                  placeholderTextColor={Color.ICON_COLOR}
                  onChangeText={(text) => {
                    this.setState({passwordConfirm: text});
                  }}
                  value={this.state.passwordConfirm}
                  secureTextEntry={true}
                />
              </View>
              <View style={{height: 60}} />
              <View
                style={{
                  flexDirection: 'row',
                  height: 50,
                  alignContent: 'center',
                  alignItems: 'center',
                  backgroundColor: Color.BACKGROUND_COLOR,
                  borderRadius: 50 / 2,
                }}>
                <TouchableOpacity
                  style={{flex: 1}}
                  onPress={() => this.reset()}>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 16,
                      alignSelf: 'center',
                    }}>
                    {text.confirm}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </ImageBackground>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  upperPortion: {
    margin: 10,
    flex: 1,
  },
  bottomPortion: {
    margin: 10,
    flex: 2,

    justifyContent: 'center',
    alignItems: 'center',
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});
