import React, {Component} from 'react';
import {
  ScrollView,
  //SafeAreaView,
  View,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Text,
  TextInput,
  Image,
  KeyboardAvoidingView,
} from 'react-native';
import Icon from 'react-native-easy-icon';
import Color from '../../constants/Color';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import base64 from 'react-native-base64';
import {fetchUser} from './api';
import text from '../../constants/strings';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {phoneNumber: '', password: '', loader: false};
  }

  validateFields() {
    if (this.state.phoneNumber === '') {
      Snackbar.show({
        text: text.phonError1,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }
    if (this.state.password === '') {
      Snackbar.show({
        text: text.passwordError1,
        duration: Snackbar.LENGTH_SHORT,
      });
      return false;
    }

    return true;
  }

  userLogin = async () => {
    const {phoneNumber, password} = this.state;
    if (this.validateFields()) {
      this.setState({loader: true});
      let user = {
        AUTHENTICATEUSER: {
          USERNAME: phoneNumber,
          PASSWORD: password,
        },
      };
      console.log(user);
      const res = await fetchUser(user);
      console.log(res);
      if (res.code === 200) {
        this.setState({loader: false});
        let json = res.json.RESPONSEINFO;
        if (json.STATUS.ERRORNO === '0') {
          let data = json.USERINFO;
          data.PASSWORD = base64.encode(password);
          //console.log(data);
          await AsyncStorage.setItem('session', JSON.stringify(data));
          this.props.navigation.replace('homeScreen');
        } else if (json.STATUS.ERRORNO === '110521') {
          Snackbar.show({
            text: text.code110521,
            duration: Snackbar.LENGTH_SHORT,
          });
        } else if (json.STATUS.ERRORNO === '40001') {
          Snackbar.show({
            text: text.code40001,
            duration: Snackbar.LENGTH_SHORT,
          });
        }
      } else {
        this.setState({loader: false});
        Snackbar.show({
          text: text.somethingWrong,
          duration: Snackbar.LENGTH_SHORT,
        });
      }
    }
  };

  render() {
    return (
      <ScrollView
        contentContainerStyle={{flexGrow: 1, backgroundColor: 'black'}}
        keyboardShouldPersistTaps="handled"
        bounces={false}>
        <Spinner visible={this.state.loader} textContent={''} />
        <KeyboardAvoidingView
          behavior="padding"
          style={{flexGrow: 1, backgroundColor: 'black'}}>
          <View style={styles.container}>
            <ImageBackground
              source={require('../../../assets/login_bg.png')}
              style={styles.image}>
              <View style={styles.upperPortion}>
                <View
                  style={{
                    justifyContent: 'center',
                    flex: 1,
                    alignItems: 'center',
                  }}>
                  <Image
                    style={{width: 150, height: 150}}
                    source={require('../../../assets/ICON.png')}
                  />
                </View>
              </View>
              <View style={styles.bottomPortion}>
                <View
                  style={{
                    flexDirection: 'row',
                    height: 50,
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: 50 / 2,
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: 10}}
                    name={'phone'}
                    type="material-community"
                    size={20}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1, color: 'white'}}
                    placeholder={text.placeholderPhone}
                    onChangeText={(text) => {
                      this.setState({phoneNumber: text});
                    }}
                    keyboardType="phone-pad"
                    value={this.state.phoneNumber}
                    placeholderTextColor={Color.ICON_COLOR}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    height: 50,
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: 50 / 2,
                    marginTop: 15,
                  }}>
                  <Icon
                    style={{alignSelf: 'center', margin: 10}}
                    name={'lock-outline'}
                    type="material-community"
                    size={20}
                    color={Color.ICON_COLOR}
                  />
                  <TextInput
                    style={{flex: 1, color: 'white'}}
                    placeholder={text.placeholderPassword}
                    onChangeText={(text) => {
                      this.setState({password: text});
                    }}
                    placeholderTextColor={Color.ICON_COLOR}
                    value={this.state.password}
                    secureTextEntry={true}
                  />
                </View>

                <TouchableOpacity
                  style={{marginTop: 10, alignSelf: 'flex-end'}}
                  onPress={() =>
                    this.props.navigation.navigate('ResetPassword')
                  }>
                  <Text style={{color: 'white', fontSize: 16}}>
                    {text.forgotPass}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    this.userLogin();
                  }}>
                  <View style={styles.button}>
                    <Text
                      style={{
                        color: 'white',
                        textAlign: 'center',
                        fontSize: 16,
                      }}>
                      {text.login}
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('Register')}>
                  <View style={styles.button}>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: 16,
                        alignSelf: 'center',
                      }}>
                      {text.signUp}
                    </Text>
                  </View>
                </TouchableOpacity>
                <View style={{height: 60}} />
              </View>
            </ImageBackground>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  upperPortion: {
    margin: 10,
    flex: 1,
  },
  bottomPortion: {
    margin: 10,
    flex: 2,
  },
  button: {
    flexDirection: 'row',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Color.BACKGROUND_COLOR,
    borderRadius: 50 / 2,
    marginTop: 15,
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});
