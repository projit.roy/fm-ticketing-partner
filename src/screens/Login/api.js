import {loginUrl, headers} from '../../config';
import {fetchUtil} from '../../util';
import UUIDGenerator from 'react-native-uuid-generator';

export const fetchUser = async (user) => {
  let id = new Date().getTime();
  await UUIDGenerator.getRandomUUID().then((uuid) => {
    id = uuid;
  });
  const url = `${loginUrl}?ReferenceNo=${id}`;
  return fetchUtil(url, 'Post', headers, JSON.stringify(user));
};
