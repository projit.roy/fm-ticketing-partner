import React, {Component} from 'react';
import {
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  Image,
  TouchableOpacity,
  View,
  Platform,
} from 'react-native';
import normalize from 'react-native-normalize';
import Icon from 'react-native-easy-icon';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Spinner from 'react-native-loading-spinner-overlay';
import Snackbar from 'react-native-snackbar';
import {movieList, movieBooking} from './api';
import Color from '../constants/Color';
import text from '../constants/strings';

export default class ReviewScreenSaleOne extends Component {
  constructor(props) {
    super(props);

    this.state = {
      movieName: props.route.params.item.MOVIE,
      movieGenere: '(Telugu)',
      expiry: props.route.params.item.VALIDITY,
      saleQuantity: [props.route.params.item.balance],
      phoneNumber: '',
      price: props.route.params.item.price,
      selectedQuantity: '1',
      loader: false,
    };
  }

  componentDidMount() {
    this.getDetails();
  }

  getDetails = () => {
    this.setState({loader: true});
    AsyncStorage.getItem('session').then(async (value) => {
      if (value !== null) {
        const data = JSON.parse(value);
        const user = {
          KEY_NAMEVALUE: {
            KEY_NAME: 'PROCESS',
            KEY_VALUE: 'DISTRIBUTORMOVIELIST',
          },
          ADDITIONAL_INFO: JSON.stringify({
            DISTRIBUTORID: data.REF_ID,
            MOVIECODE: this.props.route.params.item.MOVIECODE,
          }),
        };
        console.log(user);
        const res = await movieList(user);
        if (res.code === 200) {
          console.log(res);
          let json = res.json.RESPONSEINFO;
          if (json.STATUS.ERRORNO === '0') {
            this.setState({loader: false});
            let data = json.DISTRIBUTORMOVIELIST;
            console.log(data);
            this.setState({
              ticketPrice: data[0].AMOUNT,
              posterPrice: data[1].AMOUNT,
              minTicket: data[0].MINQUANTITY,
              minT: data[0].MINQUANTITY,
              minP: data[1].MINQUANTITY,
              minPoster: data[1].MINQUANTITY,
              data: res.json.RESPONSEINFO.DISTRIBUTORMOVIELIST,
            });
          }
        }
      }
    });
  };

  bookMovie = () => {
    this.setState({loader: true});
    AsyncStorage.getItem('session').then(async (value) => {
      if (value !== null) {
        const user = JSON.parse(value);
        const {
          data,
          minTicket,
          ticketPrice,
          posterPrice,
          minPoster,
        } = this.state;
        const bookInfo = [
          data[0].MOVIECODE +
            ':' +
            minTicket +
            ':' +
            Number(ticketPrice) * Number(minTicket) +
            ':Y',
        ];
        Number(minPoster) > 0
          ? bookInfo.push(
              data[1].MOVIECODE +
                ':' +
                minPoster +
                ':' +
                Number(posterPrice) * Number(minPoster) +
                ':Y',
            )
          : null;
        console.log(bookInfo);
        const param = {
          KEY_NAMEVALUE: {
            KEY_NAME: 'PROCESS',
            KEY_VALUE: 'DISTRIBUTORBOOKINGREQUEST',
          },
          ADDITIONAL_INFO: JSON.stringify({
            DISTRIBUTOR_ID: user.REF_ID,
            BOOKING_INFO: bookInfo,
          }),
        };
        console.log(param);
        const res = await movieList(param);
        console.log(res);
        if (res.code === 200) {
          let json = res.json.RESPONSEINFO;
          if (json.STATUS.ERRORNO === '0') {
            this.setState({loader: false});
            this.props.navigation.navigate('ReviewBuyTwo', {
              movie: this.state.movieName,
              genere: this.state.movieGenere,
              quantity: this.state.minTicket,
              price: this.state.ticketPrice,
              expiry: this.state.expiry,
              additional:
                Number(minPoster) === 0 || Number(minTicket) === 0
                  ? [json.DISTRIBUTORBOOKINGREQUES.ADDITIONAL_INFO]
                  : json.DISTRIBUTORBOOKINGREQUES.ADDITIONAL_INFO,
            });
          } else if (json.STATUS.ERRORNO === '113029') {
            this.setState({loader: false});
            Snackbar.show({
              text: text.code113029,
              duration: Snackbar.LENGTH_SHORT,
            });
          } else {
            this.setState({loader: false});
          }
        }
      }
    });
  };

  changeTicket = (step) => {
    const {minTicket, minT} = this.state;
    if (step === 'add') {
      this.setState({minTicket: Number(minTicket) + 1});
    } else if (step === 'minus') {
      if (Number(minT) < Number(minTicket)) {
        this.setState({minTicket: Number(minTicket) - 1});
      }
    }
  };

  changePoster = (step) => {
    const {minPoster, minP} = this.state;
    if (step === 'add') {
      this.setState({minPoster: Number(minPoster) + 1});
    } else if (step === 'minus') {
      if (Number(minP) < Number(minPoster)) {
        this.setState({minPoster: Number(minPoster) - 1});
      }
    }
  };

  render() {
    return (
      <ScrollView contentContainerStyle={{flexGrow: 1}} bounces={false}>
        <Spinner visible={this.state.loader} textContent="" />
        <View style={styles.container}>
          <ImageBackground
            source={require('../../assets/asset-12.png')}
            style={styles.image}>
            <View style={{flex: 1}}>
              <View style={styles.upperPortion}>
                <TouchableOpacity
                  style={{marginTop: Platform.OS === 'ios' ? 20 : 0}}
                  onPress={() => {
                    this.props.navigation.goBack();
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      alignSelf: 'flex-start',
                    }}>
                    <Icon
                      name={'chevron-back'}
                      type="ionicon"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View
                  style={{
                    justifyContent: 'center',
                    flex: 1,
                    alignItems: 'center',
                  }}
                />
              </View>
              <View style={styles.bottomPortion}>
                <Text
                  style={{
                    fontSize: normalize(18),
                    color: 'white',
                    alignSelf: 'center',
                  }}>
                  {this.state.movieName}
                </Text>
                <Text
                  style={{
                    fontSize: normalize(14),
                    color: 'white',
                    alignSelf: 'center',
                  }}>
                  {this.state.movieGenere}
                </Text>
                <View style={{height: normalize(20)}} />

                <View style={{flexDirection: 'row', alignSelf: 'center'}}>
                  <Text style={{fontSize: normalize(14), color: 'white'}}>
                    {text.expiryDate}
                  </Text>
                  <View style={{width: normalize(8)}} />
                  <Text
                    style={{
                      fontSize: normalize(14),
                      color: 'white',
                      alignSelf: 'center',
                    }}>
                    {this.state.expiry}
                  </Text>
                </View>
                <View style={{height: normalize(30)}} />
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      width: '50%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{fontSize: 15, color: 'white'}}>
                      {text.ticketPrice}
                    </Text>
                  </View>
                  <View
                    style={{
                      width: '50%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{fontSize: 15, color: 'white'}}>
                      {text.minTicket}
                      {this.state.minT})
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      width: '50%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontSize: 18,
                        color: 'white',
                        marginTop: 15,
                        marginBottom: 15,
                      }}>
                      ₹{this.state.ticketPrice}
                    </Text>
                  </View>
                  <View
                    style={{
                      width: '50%',
                      justifyContent: 'space-around',
                      alignItems: 'center',
                      flexDirection: 'row',
                    }}>
                    <TouchableOpacity
                      onPress={() => this.changeTicket('minus')}>
                      <Image
                        style={{width: 20, height: 20, tintColor: 'white'}}
                        source={require('../../assets/minus.png')}
                      />
                    </TouchableOpacity>
                    <Text style={{fontSize: 18, color: 'white'}}>
                      {this.state.minTicket}
                    </Text>
                    <TouchableOpacity onPress={() => this.changeTicket('add')}>
                      <Image
                        style={{width: 20, height: 20, tintColor: 'white'}}
                        source={require('../../assets/add.png')}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View>
                  <View style={{flexDirection: 'row'}}>
                    <View
                      style={{
                        width: '50%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text style={{fontSize: 15, color: 'white'}}>
                        {text.posterPrice}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: '50%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text style={{fontSize: 15, color: 'white'}}>
                        {text.minPoster}
                        {this.state.minP})
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      width: '50%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontSize: 18,
                        color: 'white',
                        marginTop: 15,
                        marginBottom: 15,
                      }}>
                      ₹{this.state.posterPrice}
                    </Text>
                  </View>
                  <View
                    style={{
                      width: '50%',
                      justifyContent: 'space-around',
                      alignItems: 'center',
                      flexDirection: 'row',
                    }}>
                    <TouchableOpacity
                      onPress={() => this.changePoster('minus')}>
                      <Image
                        style={{width: 20, height: 20, tintColor: 'white'}}
                        source={require('../../assets/minus.png')}
                      />
                    </TouchableOpacity>
                    <Text style={{fontSize: 18, color: 'white'}}>
                      {this.state.minPoster}
                    </Text>
                    <TouchableOpacity onPress={() => this.changePoster('add')}>
                      <Image
                        style={{width: 20, height: 20, tintColor: 'white'}}
                        source={require('../../assets/add.png')}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <TouchableOpacity
                style={{
                  position: 'absolute',
                  bottom: 0,
                  width: '100%',
                }}
                onPress={() => {
                  this.bookMovie();
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(65),
                    justifyContent: 'center',
                    backgroundColor: Color.BACKGROUND_COLOR,
                  }}>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: normalize(20),
                      alignSelf: 'center',
                    }}>
                    {text.reviewOrder}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  upperPortion: {
    margin: normalize(10),
    flex: 1,
  },
  bottomPortion: {
    margin: normalize(10),
    flex: 2,
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});
