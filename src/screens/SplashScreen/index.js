import React, {Component} from 'react';
import {View, ImageBackground, Image} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class SplashScreen extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    setTimeout(() => {
      this.checkSession();
    }, 3000);
  }

  checkSession = () => {
    AsyncStorage.getItem('session').then((value) => {
      if (value !== null) {
        this.props.navigation.navigate('homeScreen');
      } else {
        this.props.navigation.replace('Login');
      }
    });
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <ImageBackground
          source={require('../../../assets/login_bg.png')}
          style={{
            flex: 1,
            justifyContent: 'center',
            resizeMode: 'cover',
          }}>
          <View
            style={{
              justifyContent: 'center',
              flex: 1,
              alignItems: 'center',
            }}>
            <Image
              style={{width: 150, height: 150, marginBottom: 10}}
              source={require('../../../assets/ICON.png')}
            />
          </View>
        </ImageBackground>
      </View>
    );
  }
}
