import React, {Component} from 'react';
import {
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Platform,
} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import Snackbar from 'react-native-snackbar';
import normalize from 'react-native-normalize';
import Icon from 'react-native-easy-icon';
import Spinner from 'react-native-loading-spinner-overlay';
import Color from '../constants/Color';
import text from '../constants/strings';

export default class ReviewScreenSaleOne extends Component {
  constructor(props) {
    super(props);
    //console.log(props.route);
    this.state = {
      movieName: props.route.params.movieName,
      movieGenere: props.route.params.movieGenere,
      expiry: props.route.params.expiryDate,
      saleQuantity: [props.route.params.balance],
      phoneNumber: '',
      price: props.route.params.price,
      selectedQuantity: '1',
      loader: false,
      movieCode: props.route.params.movieCode,
    };
  }

  render() {
    return (
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <Spinner visible={this.state.loader} textContent="" />
        <View style={styles.container}>
          <ImageBackground
            source={require('../../assets/asset-12.png')}
            style={styles.image}>
            <View style={{flex: 1}}>
              <View style={styles.upperPortion}>
                <TouchableOpacity
                  style={{marginTop: Platform.OS === 'ios' ? 20 : 0}}
                  onPress={() => {
                    this.props.navigation.goBack();
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      alignSelf: 'flex-start',
                    }}>
                    <Icon
                      name={'chevron-back'}
                      type="ionicon"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View
                  style={{
                    justifyContent: 'center',
                    flex: 1,
                    alignItems: 'center',
                  }}
                />
              </View>
              <View style={styles.bottomPortion}>
                <Text
                  style={{
                    fontSize: normalize(18),
                    color: 'white',
                    alignSelf: 'center',
                  }}>
                  {this.state.movieName}
                </Text>
                <Text
                  style={{
                    fontSize: normalize(14),
                    color: 'white',
                    alignSelf: 'center',
                  }}>
                  {this.state.movieGenere}
                </Text>
                <View style={{height: normalize(20)}} />

                <View style={{flexDirection: 'row', alignSelf: 'center'}}>
                  <Text style={{fontSize: normalize(14), color: 'white'}}>
                    {text.expiryDate}
                  </Text>
                  <View style={{width: normalize(8)}} />
                  <Text
                    style={{
                      fontSize: normalize(14),
                      color: 'white',
                      alignSelf: 'center',
                    }}>
                    {this.state.expiry}
                  </Text>
                </View>
                <View style={{height: normalize(10)}} />
                <Text
                  style={{
                    fontSize: normalize(14),
                    color: Color.ICON_COLOR,
                    alignSelf: 'center',
                  }}>
                  {' '}
                  {text.sellNoticePart1 +
                    (Number(this.state.saleQuantity) > 6
                      ? 6
                      : this.state.saleQuantity) +
                    text.sellNoticePart2}
                </Text>

                <View style={{height: normalize(30)}} />
                <View
                  style={{
                    backgroundColor: Color.BACKGROUND_COLOR,
                    borderRadius: normalize(5),
                    padding: normalize(16),
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      height: normalize(40),
                      backgroundColor: Color.ICON_COLOR,
                      borderRadius: normalize(40) / 2,
                    }}>
                    <Icon
                      style={{alignSelf: 'center', margin: normalize(10)}}
                      name={'phone'}
                      type="material-community"
                      size={normalize(20)}
                      color={Color.BACKGROUND_COLOR}
                    />
                    <TextInput
                      style={{flex: 1, color: Color.BACKGROUND_COLOR}}
                      placeholder={text.placeholderPhone}
                      onChangeText={(text) => {
                        this.setState({phoneNumber: text});
                      }}
                      value={this.state.phoneNumber}
                      placeholderTextColor={Color.BACKGROUND_COLOR}
                    />
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      height: normalize(40),
                      backgroundColor: Color.ICON_COLOR,
                      borderRadius: normalize(40) / 2,
                      marginTop: normalize(15),
                    }}>
                    <Icon
                      style={{alignSelf: 'center', margin: normalize(10)}}
                      name={'ios-pricetags'}
                      type="ionicon"
                      size={normalize(20)}
                      color={Color.BACKGROUND_COLOR}
                    />
                    {Platform.OS === 'ios' ? (
                      <TextInput
                        style={{flex: 1, color: Color.BACKGROUND_COLOR}}
                        //placeholder="Phone Number"
                        keyboardType="number-pad"
                        onChangeText={(text) => {
                          this.setState({selectedQuantity: text});
                        }}
                        value={this.state.selectedQuantity}
                        placeholderTextColor={Color.BACKGROUND_COLOR}
                      />
                    ) : (
                      <Picker
                        selectedValue={this.state.selectedQuantity}
                        style={{height: normalize(40), flex: 1}}
                        mode={'dropdown'}
                        onValueChange={(itemValue, itemIndex) =>
                          this.setState({selectedQuantity: itemValue})
                        }>
                        {Array.from(
                          Array(Number(this.state.saleQuantity)).keys(),
                        ).map((item) => (
                          <Picker.Item
                            key={item + 1}
                            label={`${item + 1}`}
                            value={item + 1}
                          />
                        ))}
                      </Picker>
                    )}
                  </View>
                </View>
              </View>

              <TouchableOpacity
                style={{
                  position: 'absolute',
                  bottom: 0,
                  width: '100%',
                }}
                onPress={() => {
                  console.log(
                    this.state.selectedQuantity,
                    this.state.saleQuantity,
                  );
                  if (this.state.phoneNumber.length !== 10) {
                    Snackbar.show({
                      text: text.phoneError2,
                      duration: Snackbar.LENGTH_SHORT,
                    });
                  } else {
                    if (Number(this.state.selectedQuantity) > 0) {
                      if (Number(this.state.selectedQuantity) > 6) {
                        Snackbar.show({
                          text: text.sellError1,
                          duration: Snackbar.LENGTH_SHORT,
                        });
                      } else {
                        this.props.navigation.navigate('ReviewSaleTwo', {
                          movie: this.state.movieName,
                          genere: this.state.movieGenere,
                          phoneNumber: this.state.phoneNumber,
                          quantity: this.state.selectedQuantity,
                          price: this.state.price,
                          expiry: this.state.expiryDate,
                          movieCode: this.state.movieCode,
                        });
                      }
                    } else {
                      Snackbar.show({
                        text: text.sellError2,
                        duration: Snackbar.LENGTH_SHORT,
                      });
                    }
                  }
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    height: normalize(45),
                    justifyContent: 'center',
                    backgroundColor: Color.BACKGROUND_COLOR,
                  }}>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: normalize(16),
                      alignSelf: 'center',
                    }}>
                    {text.reviewOrder}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  upperPortion: {
    margin: normalize(10),
    flex: 1,
  },
  bottomPortion: {
    margin: normalize(10),
    flex: 2,
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});
