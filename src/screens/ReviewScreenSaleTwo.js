import React, {Component} from 'react';
import {
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform,
} from 'react-native';
import Icon from 'react-native-easy-icon';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Snackbar from 'react-native-snackbar';
import Color from '../constants/Color';
import normalize from 'react-native-normalize';
import {Picker} from '@react-native-picker/picker';
import Spinner from 'react-native-loading-spinner-overlay';
import {sale, getDistributorDetails} from '../screens/api';
import text from '../constants/strings';

export default class ReviewScreenSaleTwo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      movieName: props.route.params.movie,
      movieGenere: props.route.params.genere,
      expiry: props.route.params.expiry,
      saleQuantity: props.route.params.quantity,
      price: props.route.params.price,
      phoneNumber: props.route.params.phoneNumber,
      selectedQuantity: '5',
      loader: false,
      movieCode: props.route.params.movieCode,
    };
  }

  createTable = () => {
    const items = [];
    // Outer loop to create parent
    for (let i = 0; i < Number(this.state.saleQuantity); i++) {
      items.push(<Picker.Item label={i.toString()} value={i.toString()} />);
    }
    //Create the parent and add the children
    console.debug(items);
    return items;
  };

  saleTicket = () => {
    this.setState({loader: true});
    AsyncStorage.getItem('session').then((value) => {
      let id = JSON.parse(value);
      console.log(id);
      const user = {
        KEY_NAMEVALUE: {
          KEY_NAME: 'PROCESS',
          KEY_VALUE: 'DISTRIBUTORDETAILS',
        },
        ADDITIONAL_INFO: JSON.stringify({USERNAME: id.USER_NAME}),
      };
      getDistributorDetails(user).then((res) => {
        console.log(res);
        if (res.code === 200) {
          let json = res.json.RESPONSEINFO;
          if (json.STATUS.ERRORNO === '0') {
            const info = json.DISTRIBUTORDETAILS;
            const data = {
              KEY_NAMEVALUE: {
                KEY_NAME: 'PROCESS',
                KEY_VALUE: 'DISTRIBUTORSELLREQUEST',
              },
              ADDITIONAL_INFO: JSON.stringify({
                DISTRIBUTORID: info.ENTITY_ID,
                MOVIE_NAME: this.state.movieCode,
                CUSTOMER_MOBILE_NUMBER: this.state.phoneNumber,
                QUANTITY: this.state.saleQuantity,
                TOTAL_PRICE:
                  Number(this.state.saleQuantity) * Number(this.state.price),
              }),
            };
            console.log(data);
            sale(data).then((res) => {
              console.log(res);
              if (res.code === 200) {
                let json = res.json.RESPONSEINFO;
                if (json.STATUS.ERRORNO === '0') {
                  this.setState({loader: false});
                  Snackbar.show({
                    text: text.ticketSaleSuccess,
                    duration: Snackbar.LENGTH_SHORT,
                  });
                  this.props.navigation.navigate('Sale');
                }
              }
            });
          }
        }
      });
    });
  };

  render() {
    return (
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <Spinner visible={this.state.loader} textContent="" />
        <View style={styles.container}>
          <ImageBackground
            source={require('../../assets/asset-12.png')}
            style={styles.image}>
            <View style={{flex: 1}}>
              <View style={styles.upperPortion}>
                <TouchableOpacity
                  style={{marginTop: Platform.OS === 'ios' ? 20 : 0}}
                  onPress={() => {
                    this.props.navigation.goBack();
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      alignSelf: 'flex-start',
                    }}>
                    <Icon
                      name={'chevron-back'}
                      type="ionicon"
                      size={normalize(26)}
                      color="white"
                    />
                  </View>
                </TouchableOpacity>
                <View
                  style={{
                    justifyContent: 'center',
                    flex: 1,
                    alignItems: 'center',
                  }}
                />
              </View>
              <View style={styles.bottomPortion}>
                <View style={styles.item}>
                  <View
                    style={{
                      marginStart: normalize(8),
                      backgroundColor: Color.BACKGROUND_COLOR,
                      borderRadius: normalize(5),
                      padding: normalize(8),
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        flex: 1,
                        justifyContent: 'center',
                      }}>
                      <Text style={styles.title}>{this.state.phoneNumber}</Text>
                    </View>
                    <Text style={styles.subHeading}>
                      {this.state.movieName}
                    </Text>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <View style={{flex: 1}}>
                        <View style={{flex: 1, flexDirection: 'row'}}>
                          <Text
                            style={{fontSize: normalize(12), color: 'white'}}>
                            {text.ticketQuantity}
                          </Text>
                          <Text
                            style={{fontSize: normalize(12), color: 'white'}}>
                            {this.state.saleQuantity}
                          </Text>
                        </View>
                      </View>
                      <View>
                        <View
                          style={{
                            flex: 1,
                            position: 'absolute',
                            bottom: 0,
                            alignSelf: 'flex-end',
                          }}>
                          {/*<View*/}
                          {/*  style={{*/}
                          {/*    flexDirection: 'row',*/}
                          {/*    justifyContent: 'flex-end',*/}
                          {/*  }}>*/}
                          {/*  <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>*/}
                          {/*    Sale Date :*/}
                          {/*  </Text>*/}
                          {/*  <Text style={{fontSize: normalize(12), color: Color.ICON_COLOR}}>*/}
                          {/*    15/11/2020*/}
                          {/*  </Text>*/}
                          {/*</View>*/}

                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'center',
                              justifyContent: 'flex-end',
                            }}>
                            <Text
                              style={{
                                fontSize: normalize(14),
                                color: 'white',
                              }}>
                              {text.totalPrice}
                            </Text>
                            <View style={{width: normalize(5)}} />
                            <Text
                              style={{
                                fontSize: normalize(12),
                                color: 'white',
                              }}>
                              ₹{' '}
                              {Number(this.state.saleQuantity) *
                                Number(this.state.price)}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              <View
                style={{
                  position: 'absolute',
                  bottom: 0,
                  width: '100%',
                }}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    height: normalize(45),
                    justifyContent: 'center',
                    backgroundColor: 'green',
                  }}>
                  <Text
                    style={{
                      marginStart: normalize(5),
                      color: 'white',
                      fontSize: normalize(16),
                      flex: 1,
                      alignSelf: 'center',
                    }}>
                    {text.totalTicket}
                  </Text>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: normalize(24),
                      flex: 1,
                      alignSelf: 'center',
                    }}>
                    {this.state.saleQuantity}
                  </Text>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: normalize(16),
                      flex: 1,
                      alignSelf: 'center',
                    }}>
                    {text.totalPrice}
                  </Text>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: normalize(24),
                      flex: 1,
                      alignSelf: 'center',
                    }}>
                    ₹
                    {Number(this.state.saleQuantity) * Number(this.state.price)}
                  </Text>
                </View>
                <TouchableOpacity
                  style={{
                    bottom: 0,
                    width: '100%',
                  }}
                  onPress={() => {
                    this.saleTicket();
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      height: normalize(45),
                      justifyContent: 'center',
                      backgroundColor: Color.BACKGROUND_COLOR,
                    }}>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: normalize(16),
                        alignSelf: 'center',
                      }}>
                      {text.sale}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </ImageBackground>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  upperPortion: {
    margin: normalize(10),
    flex: 1,
  },
  bottomPortion: {
    margin: normalize(10),
    flex: 2,
  },

  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  item: {
    backgroundColor: Color.APP_YELLOW,
    borderRadius: 5,

    marginVertical: 8,
    marginHorizontal: 8,
  },
  title: {
    flex: 1,
    fontSize: normalize(20),
    color: Color.APP_YELLOW,
  },
  subHeading: {
    fontSize: normalize(14),
    color: 'white',
  },
});
