import LocalizedStrings from 'react-native-localization';

export const LANGUAGES = ['fr', 'en'];

const Strings = new LocalizedStrings({
  en: {
    forgotPass: 'Forgot Password',
    login: 'LOGIN',
    signUp: 'SIGN UP',
    signOut: ' Sign out',
    resetPass: 'Reset Password',
    confirm: 'CONFIRM',
    update: 'Update',
    resend: 'RESEND',
    aggree: 'I agree to the',
    terms: 'Terms & Condition ',
    and: 'and',
    policy: 'Privacy Policy',
    paymentTermsHeader: 'Payment Terms',
    about: 'About Us',
    aboutTicketPartner: 'About Ticketing Partner',
    helpHeader: 'Help',
    buyMovie: 'BUY MOVIES',
    aboutUs:
      'Friday Movies is a video-on-demand Pay-Per-View platform owned by Digital Celluloid Private Limited. Our adaptive and highly evolved video streaming technology ensures the best protected video quality based on the top-end bandwidth availability. A high attention to quality of experience across devices and platforms make Friday Movies the hotspot destination for Any Time Theatre (ATT) consumers. The most exclusive movies, independent films and short films – Friday Movies is your destination for them all. Get one ticket, and watch your favourite film with your family, friends or both. Pause where you like, watch when you like. And you can watch them on TV, mobile, computer, tablet, wherever you like on a friendly user interface. If you like the film, you can gift the movies with a personalized message. What’s more, with Friday Movies you can interact with your favourite stars, get giveaways, offers, lucky draws and so much more. So get it, and start watching your favourite movie now!',
    userAvailable: 'User Already Registered',
    userNotAvailable: 'User Not Found',
    purchased: 'Purchased : ',
    purchasedDate: 'Purchased Date :',
    sellDate: 'Sell Date :',
    orderHistory: 'Order History',
    sellHistory: 'Sell History',
    myAccount: 'My Account',
    sold: 'Sold :',
    balance: 'Balance :',
    ticket: ' Tickets',
    ticketPrice: 'Ticket Price',
    ticketQuantity: 'Ticket Quantity :',
    posterPrice: 'Poster Price',
    minTicket: ' Tickets Quantity (min ',
    minPoster: ' Posters Quantity (min ',
    noData: 'No Data Found',
    reviewOrder: ' Review Order',
    dashboard: 'Dashboard',
    sale: 'Sell',
    account: 'Account',
    help: 'For support & Queries:\t\n Email:\ndeveloper@digitalcelluloid.in',
    poster: 'Posters',
    expiryDate: ' Expiry Date :',
    orderId: 'Order Id :',
    paymentTerms:
      'Once payment made are nonrefundable or non-cancellable for any reason. Transaction fees charged would be borne by cardholder for any payment (if applicable ). Transaction fee charges would not be refunded/ reversed under any circumstances for any refund/ reversal /chargeback and any other reasons(if applicable ).',
    passwordChangedSuccess: 'Password Changed Successfully',
    totalPrice: 'Total Price:',
    gst: 'GST:',
    charges: 'Transaction Charges:',
    totalTicketPrice: 'Total Tickets Price',
    totalPay: 'Total Pay',
    totalTicket: 'Total Tickets :',
    orderNow: 'Order Now',
    sellNoticePart1: 'You can sale ',
    sellNoticePart2: ' tickets in a single transition.',
    noTicket: 'No Ticket Available for sale',
    otpResent: 'OTP Resent',
    otpMessage: 'OTP sent to your Mobile Number',
    placeholderOtp: 'Enter OTP',
    placeholderName: 'Enter Name',
    placeholderPhone: 'Phone Number',
    placeholderPassword: 'Password',
    placeholderConfirmPassword: 'Confirm Password',
    placeholderAddress: 'Address',
    placeholderCity: 'City',
    placeholderPincode: 'Pincode',
    placeholderTrade: 'Trade Name(Optional)',
    placeholderPan: 'Pan Number(Optional)',
    placeholderGst: 'GST Number(Optional)',
    phonError1: 'Please Enter Phone Number',
    phoneError2: 'Please Enter a valid Phone Number',
    passwordError1: 'Please Enter Password',
    passwordError2: 'Password length should be greater than 7',
    passwordError3: 'Password and Confirmed Password not matched',
    passwordError4: 'Please Enter Confirm Password',
    addressError: 'Please Enter your address',
    cityError: 'Please Enter City Name',
    pincodeError: 'Please Enter Your Pincode',
    panError: 'Pan Number should have 10 digit',
    gstError: 'GST should have 15 digit',
    termsError: 'Please accept Terms & Conditions',
    userError1: 'Please Enter Username',
    userError2: 'Please Enter at least 3 character in userName',
    sellError1: 'You Can Sell Maximum 6 Tickets',
    sellError2: 'Please select Atleast 1 Ticket',
    otpError: 'Invalid OTP',
    code110521: 'Invalid Mobile Number',
    code40001: 'Invalid Username/Password',
    code40030: 'Please should not matched with the previous password',
    code113029: 'Minimum Order Amount 200',
    code40111: 'Entered OTP is not valid',
    somethingWrong: 'Something went wrong!!Please try again',
    successUser: 'User Created Successfully',
    ticketBuySucess: 'Ticket successfully booked',
    ticketSaleSuccess: 'Ticker sold Successfully',
    dataUpdateSuccess: 'Data Updated Successfully',
    termsCondition: `
    <!DOCTYPE html>
    <html>
    <style type="text/css">
    body{margin:0; padding:0;}
      .bg{
        width: 100%;
        height: initial;
        background-image: url(bg.png);
        background-repeat: no-repeat;
        background-position: center;
        background-size: cover;
      }
      .bg_wrap{
        padding: 40px 30px;
      }
      .bg_wrap h1{
        font-size: 34px;
        color: #fff;
        font-weight: 500;
        display: block;
        text-align: center;
      }
      .bg_wrap p{
        font-size: 16px;
        color: #fff;
      }
      .bg_wrap h2{
        font-size: 26px;
        color: #fff;
      }
      .bg_wrap h3{
        font-size: 22;
        color: #fff;
      }
      .bg_wrap ul{
        margin: 0;
        padding: 0;
      }
      .bg_wrap ul li{
        font-size: 16px;
        color: #fff;
        
      }
      .bg_wrap ol{
        margin: 0;
        padding: 0;
      }
      .bg_wrap ol li{
        font-size: 16px;
        color: #fff;
        
      }
    </style>
    <body>
    
      <div class="bg">
        <div class="bg_wrap">
        <h1>Terms of Service</h1>
    
    <p>
        Digital Celluloid Private Limited, a company incorporated under the
        provisions of the Companies Act, 2013 (hereinafter “Us”, “We”, “Our”
        “Friday Movies” or “Company”, which expression includes its successors or
        assigns) is the owner of the web platform, i.e. www.
        developer@digitalcelluloid.in.; the mobile Application under the name and
        style “Friday Movies App” and provides to its customers, visitors and third
        parties various options vide VIDEO ON DEMAND – PAY PER VIEW on the
        platforms owned and run by the company.
    </p>
    <p>
        <strong>1. DEFINITIONS</strong>
    </p>
    <p>
        1.1 <strong>“Account Information”</strong> shall mean your information,
        including Personal Information, that the company requires in order for you
        to effectively use Our Services (as detailed below).
    </p>
    <p>
        1.2 <strong>“App/Application”</strong> shall mean the Friday Movies
        application that can be accessed and used by Authorized Users to avail the
        Services through the Authorized Devices.
    </p>
    <p>
        1.3 <strong>“Authorized Devices”</strong> shall refer to the computers,
        televisions, mobiles, tablets, and other devices that are compatible and
        permitted by the Agreement for accessing Content and Services via the App
        presently accessible to the general public and/or any such
        apparatus/gadgets which may be invented/discovered in the future and which
        are made compatible for accessing and use of the content provided by the
        company to its authorized users or to any other persons based on terms and
        conditions.
    </p>
    <p>
        1.4 <strong>“Authorized Users”</strong> shall mean the Users who have
        selected / subscribed and/or made payments for accessing content provided
        by the Company on its website/application, including any other
        information/trailers/videos provided by the company for viewing by the said
        user.
    </p>
    <p>
        1.5 <strong>“Content”</strong> shall include, but not be limited to, the
        available audio, visual and audio-visual content such as movies,
        web-series, documentaries, music, etc. that are provided to Authorized
        Users through Website and the App by the Company.
    </p>
    <p>
        1.6 <strong>“Digital Celluloid Affiliates”</strong> shall mean its
        directors, agents, officers, employees, agents, affiliates, successors,
        assigns, managers, members, subsidiaries, partners, attorneys and service
        providers (such as directors, producers, licensors).
    </p>
    <p>
        1.7 <strong>“Personal Information”</strong> shall have the meaning assigned
        to this term under the IT (Reasonable Security Practices and Procedures and
        Sensitive Personal Data or Information) Rules, 2011, and includes any
        analogous meaning(s) provided under the applicable laws of the Users.
    </p>
    <p>
        1.8 <strong>“Profile”</strong> shall refer to the account created on
        Website or on the App that contain Your Account Information as provided by
        you and any and all such details which are provide by you to the Company.
    </p>
    <p>
        1.9 <strong>“Service(s)”</strong> shall mean the access, use and/or viewing
        of content recommended, provided, displayed and/or proposed to be provided
        by the Company to its users along with any other tools; materials;
        applications; user interface; recommendations; comments; reviews; contests,
        etc. provided via Website and the App by the company.
    </p>
    <p>
        1.10 <strong>“SPD” or “Sensitive Personal Data” </strong>shall have the
        meaning assigned to this term under the IT (Reasonable Security Practices
        and Procedures and Sensitive Personal Data or Information) Rules, 2011, and
        includes any analogous meaning(s) provided under the applicable laws of the
        Users (for e.g., “Genetic Data” and “Biometric Data” defined under the
        European Union’s General Data Protection Regulation 2016/679).
    </p>
    <p>
    1.11 <strong>“User”</strong> or <strong>“You”</strong> or    <strong>“Your”</strong> or <strong>“Yours”</strong> shall refer to all
        Authorized Users of the Services, along with other users who use the
        Services in any way whatsoever, and further includes any person who
        contributes any information, content, private and public messages,
        advertisements and any other materials or services on, or towards, Website,
        App, Content and/or the Services.
    </p>
    <p>
        <strong>2. TERMS AND CONDITIONS OF SERVICE; CONSENTS:</strong>
    </p>
    <p>
        2.1 The present Terms provide the terms and conditions applicable for any
        User of Website, App, Content and/or the Services rendered by the company.
        Please note that all the terms and conditions, the privacy policy, the
        legal disclaimers and/or any other terms/policies/notes/disclaimers shall
        be read as a part and parcel of the present Terms of Service and shall be
        collectively referred to as Agreement.
    </p>
    <p>
        2.2 By installing, downloading, accessing, updating, subscribing,
        upgrading, or otherwise using the Services and Content, You consent to: (1)
        fully complying with this Agreement (2) using the Services and Content only
        as per the manner and purpose provided under this Agreement, and (3) being
        solely responsible for the transfer of any payments made through
        Website/App in exchange for accessing the Services requested by You. By
        interacting with the Website/App or otherwise availing the Services, You
        confirm that You accept this Agreement and agree to abide by the same. If
        You do not agree with the provisions of this Agreement in entirety, do not
        Interact with Website and/or the App. Further, in case of disagreement with
        the above, You are not entitled to avail, use and access any of the Content
        and Services, and any use thereafter shall be unauthorized.
    </p>
    <p>
        2.3 In order to use the Services offered by Us on Website/App, You are
        required to create a Profile and provide Us with the requested Account
        Information. You shall provide true, accurate, current and complete
        information when creating Your Profile, including the accurate depiction of
        your name, age and other mandatory details. Upon creating the Profile, You
        may use the Services subject to fully agreeing and abiding with the
        Agreement.
    </p>
    <p>
        2.4 You acknowledge and agree that Your registration details and any other
        data collected by the company is subject to the company’s Privacy Policy,
        which you have read and accept/agree to the same as a part of the present
        agreement. In order to render the Services effectively, the company may
        collect information such as language, location, contact details, address,
        time zone where the Services are used, and use these to provide content
        recommendations, in accordance with this Agreement.
    </p>
    <p>
        2.4 Content will be made available to Authorized Users upon their
        Subscription to the Services (other than free trials as provided in the
        Terms) through Website and the App, subject to this Agreement. The Company
        reserves its rights solely to display a particular content to you or not.
        In the event of any information provided by the user is found to be
        untrue/wrong, the company reserves its sole right to revoke the membership
        of the user and the company shall not be liable to make any
        refunds/payments to the user. The company is also entitled to sue the user
        for any damage and/or losses caused by the user for such wrong and untrue
        information provided by the user.
    </p>
    <p>
        2.5 If You are below the age of majority in your jurisdiction, You may use
        the Service only with the consent, guidance and supervision of a parent or
        legal guardian. If You are a minor and continue accessing the Service, it
        is assumed and admitted that You have reviewed this Agreement with Your
        parent/legal guardian and Your parent/legal guardian understands and agrees
        to it on Your behalf and that there shall be absolutely no liability on the
        part of the company for you accessing the content available and provided by
        the company. If You are below the age of majority in your jurisdiction, You
        and Your parents/legal guardians confirm that the Service is offered to You
        for Your enjoyment and these Terms shall constitute a legally binding user
        agreement between the Company and Your parents/legal guardians who are
        contracting on behalf of You. To clarify, where Users are below the age of
        majority, all references to "User", "You" and "Your" shall mean and include
        You and Your parents/legal guardians acting on Your behalf
    </p>
    <p>
        2.6 The content offered by the Company on its website and the application
        is for a varied age groups. Certain content offered on the Service may not
        be suitable for certain Users, and therefore, user discretion/parental
        discretion is advised. Also, some Content offered on the Service may not be
        appropriate for viewership by persons below the age of majority.
        Parents/legal guardians are advised to exercise discretion before allowing
        their children and/or wards to access this Service and/or any Content.
    </p>
    <p>
        <strong>3. LICENSES TO THE USERS</strong>
    </p>
    <p>
        3. 1 The subscribed user is granted a limited, revocable,
        non-sublicensable, non-exclusive and non-transferable license to access and
        use the Content and Services on Website and the application for your
        personal and non-commercial purposes, and only in accordance with the
        Agreement. The said license is limited to the non-commercial use of yours.
        The Company is authorized to restrict/block and deny access to you in the
        event of any threat or actual violation of any of the intellectual property
        rights of the company and/or any of the content provider on the
        website/application run by the company. The company shall also have
        unfettered right to take all actions against the user and/or any other
        third party to protect the intellectual property rights and/or any other
        rights with respect to the content on the company’s website/application
        provided by the Company and/or any third party to the Company.
    </p>
    <p>
        3.2 The availability and the accessibility of the content to you shall be
        at the sole discretion of the Company and on other components such as
        geographical location, etc of the user. The Company may use various
        techonogies, tools, etc to verify the geographical location for rendition
        of services. The user undertakes to depict true and accurate location at
        all times to the company.
    </p>
    <p>
        3.3 You hereby agree and acknowledge that You are accessing the Website/App
        and availing the Services at Your own risk, choice and initiative. There
        can be no liability fasted upon the company for any damages that the user
        may incur. Further, the liability if any of the company shall not exceed
        the amounts paid by the user to the company towards subscription and/or for
        any video on demand.
    </p>
    <p>
        4) <strong>INTELLECTUAL PROPERTY UNDERTAKINGS</strong>
    </p>
    <p>
        4.1 All the services provided by the Company are protected by the relevant
        intellectual property right laws, treaties and conventions, including
        rights arising for Trademark, Copyright, Patents, Designs, Trade-Secrets,
        etc.
    </p>
    <p>
        4.2 The Trademarks, copyrights and other intellectual property rights in
        the content displayed on the website and on the application are either
        owned by the Company and/or by third parties who hold all intellectual
        property rights including the copyrights or have
        transferred/licensed/assigned rights in the said to the Company,
        thereby,making the company the rightful owner of such rights. Therefore,
        you agree that you shall not create, recreate, copy, distribute, pirate,
        illegally record or make any derivative work of the content without the
        prior explicit consent of the Company and/or any other third party who is
        the rightful owner of the said content.
    </p>
    <p>
        4.3 Further, You agree that Your use of the Services and the Content gives
        You the opportunity to view, publish, transmit, submit posts and/or comment
        in accordance with these Terms. You agree that You will not misuse the
        Content and/or User material that You access and view as part of the
        Service, including without limitation, by downloading/storing the Content
        illegally or via any other non-permitted means, or infringing any of
        Company, its affiliates or licensor’s copyright, trademark, design,
        Trade-Secret and other intellectual property or proprietary rights.
    </p>
    <p>
        4.4 You agree that the Content provided on the Website/App is strictly for
        Your private viewing only and not for public exhibition (regardless of any
        charges or not for such exhibition). You hereby agree to refrain from
        further broadcasting or making available the Content provided on the
        Website/App to any third party.
    </p>
    <p>
        4.5 In case the Website/App allows You to download or stream any of the
        Content therein, You agree that You will not use, sell, transfer or
        transmit the Content to any third party; or upload the Content on any other
        website, webpage or software, directly or indirectly, such that it
        violates, or causes any damage or injury, to any rights of the company or
        its Affiliates.
    </p>
    <p>
        4.6. The website and the application, their layout, content regulation, etc
        are the intellectual property of the Company and the Company is the sole
        and exclusive rights holder. You agree to not to alter or modify any of the
        content, layout, etc within the website and the application except for the
        purposes of usage of the said websites/applications based on the
        subscription of the user.
    </p>
    <p>
        4.7 You agree to use the Services and Content, as well as otherwise act, in
        accordance with this Agreement and applicable laws, and you shall not
        attempt, facilitate, induce, aid and abet, or encourage others to act in
        violation of this Agreement.
    </p>
    <p>
        4.8 You shall not use the Services to upload, post, edit, transmit, link or
        otherwise deal with any information that is offensive, defamatory, obscene,
        vulgar, excessively violent, blasphemous, hateful, racially and ethnically
        inciteful, unlawful, threatens public health or safety, tortious, false or
        misleading, defamatory, libelous, hateful, or discriminatory, or violates
        any applicable law in force in any manner whatsoever.
    </p>
    <p>
        4.9 You shall not directly or indirectly through the use of any device,
        software, internet site, web-based service, or by any other means (1)
        remove, alter, bypass, avoid, violate, interfere with, or circumvent any
        patent, trade secret, copyright, design, trademark, or any other
        intellectual property or proprietary information relating to the Content,
        Services, Website and the App; (2) bypass any content protection or access
        control measure associated with the Content, Services, Website, and App,
        including but not limited to, geo-filtering mechanisms, parental controls,
        User information, etc; (3) Send or cause to be sent (directly or
        indirectly) unsolicited bulk messages or other unsolicited bulk
        communications of any kind through the Website or App.
    </p>
    <p>
        4.10 In the event of the Company being informed that you have been missing
        the said subscription for any other purposes as restrained under the
        present clause or agreement, the company can take all such actions,
        including injunctive reliefs to restrain you or your associates, etc from
        conducting such actions and can further claim for all such damages which
        may be sustained by the Company.
    </p>
    <p>
        4.11 Excluding Your Account Information, You acknowledge that the Website,
        App, the entire Content, Services, and all the intellectual property
        rights, including copyrights, patents, trademarks, designs, and trade
        secrets in relation to the above are solely owned by Company, its
        Affiliates and/or the company’s Licensors (“Lawful Owners”).
    </p>
    <p>
        4.12 Notwithstanding anything stated in the Agreement, the Lawful Owners
        own and retain any all intellectual and proprietary rights to the Content
        and Services throughout the territory of the world in perpetuity. These
        Terms do not transfer to You or any third party any rights, title or
        interests in, or to such, intellectual property rights, except for the
        limited license that is required to view/use the Website, Application,
        Content and Services per the Agreement.
    </p>
    <p>
        <strong>5.</strong>
        <strong>Compatible Systems</strong>
        :
    </p>
    <p>
        The Services can be availed through certain Authorized Devices which the
        Company may approve from time to time. In some cases, whether an Authorized
        Device is (or remains) capable of running a Compatible System may depend on
        the software or platform provided or maintained by the relevant device
        manufacturer or other third parties. As a result, Authorized Devices that
        run Compatible System at one time may cease to run Compatible System(s) in
        the future. The Company shall not be liable in the event of any devise, etc
        being not compatible.
    </p>
    <p>
        <strong>6. NON LIABILITY</strong>
    </p>
    <p>
        6.1 You agree that the Company shall be under no liability whatsoever in
        the event of any non-availability of the Content and Services on the
        Website/App, or any portion thereof, that is directly or indirectly caused
        due to an act of God, war, disease, revolution, riot, civil commotion,
        internet outage, strike, lockout, flood, fire, satellite failure, failure
        of any public utility, man-made disaster, or any other cause beyond the
        reasonable control of the Company.
    </p>
    <p>
        6.2 You may encounter third party applications while using the Website/App,
        including, without limitation, websites, widgets, software, services that
        interact with the Website and App. Your use of such third-party
        applications shall be subject to such third-party’s terms of use, license
        terms and privacy policies. The Company shall not be liable for any
        representations, warranties or obligations made by such third-party
        applications to You in any manner whatsoever.
    </p>
    <p>
        <strong>7 RESTRICTIONS ON USAGE:</strong>
    </p>
    <p>
        7.1 You shall not host, display, upload, modify, publish, transmit, update
        or share any information that belongs to, or relates to another User, and
        to which You have no right of access. Further, You shall not use the
        website and application and subscription for hurting any other persons
        feelings or sentiments, including religious sentiments, harass or harm
        another person, to exploit or endanger a minor, impersonate or attempt to
        impersonate, violate any other laws and/or rights of third parties.
    </p>
    <p>
        7.2 You shall not collect or use any information provided on the Website
        and App, and belonging to another User to impersonate or misrepresent such
        User, or to deceive or menace a third party, or for any commercial or
        solicitation purposes, or for facilitating/undertaking/aiding any other
        business by using the Content or Services in any manner whatsoever.
        Further, you shall not use any information (a) that is provided on the
        Website and App, and/or (b) that is belonging to other User(s), in breach
        of applicable law, including the Information Technology Act, 2000 (as
        amended) and Copyright Act, 1957 (as amended).
    </p>
    <p>
        7.3 You shall not use the Services to advertise or promote competing
        services;
    </p>
    <p>
        7.4 The Company at its sole discretion can offer for free trials and
        viewing of any content on the Company’s website and/or application
        individually or collectively and the company reserves the exclusive right
        to provide/alter/remove such free trial and viewing option at its sole
        discretion without any intimation to the user.
    </p>
    <p>
        7.5 Notwithstanding anything stated above, the company hereby reserves the
        right, at any time and in its sole discretion, to change, modify, suspend,
        or discontinue the Website/Application (in whole, or in part) or any of its
        Services (in whole, or in part), in compliance with the applicable legal
        and regulatory framework. You agree that we will not be liable to You or to
        any third party for any change, modification, suspension, or
        discontinuation of the Website or its Services, or any part of the Website
        or its Services thereof.
    </p>
    <p>
        <strong>8. MEMBERSHIP AND PAYMENTS</strong>
    </p>
    <p>
        8.1 The Content on the App and/or Website can only be availed by Authorized
        Users who conform to the provisions of this Agreement. To become an
        Authorized User, You may be required to provide Your Login Credentials that
        may contain Personal Information and Sensitive Personal Information, in
        order for you to effectively avail the Services. Further, in order to avail
        the Services as an Authorized User (“Subscription”), You may be required to
        make certain payments in the manner provided in the website/application by
        the Company. The said matter of subscription and/or the amount payable may
        vary from time to time and the company is at the sole discretion to make
        such changes. The Services can be through different subscription plans and
        Services may be accessible as free of charge, with or without
        advertisements, through payment and subscription on timeline basis, through
        one time viewing and/or pay-per-view model, without or with advertisements
        and commercials and/or through a subscription model having features of all
        such subscription models, at the sole discretion of the Company. The
        company does not guarantee the availability of any specific/minimum Content
        under any plan.
    </p>
    <p>
        8.2 The Company at its sole discretion make schemes/subscriptions for
        one-time viewing of any particular content and/or for multiple viewing of
        such content, on fixed timelines and/or for a particular time slots, etc.
    </p>
    <p>
        8.3 The company at its sole discretion may provide for various modes of
        online and offline payments for availing the services of the company. The
        said payments can be made through various sources such as debit card
        payment, credit card payment, internet banking payment and/or any other
        such payment which may be recognised and/or used as a general industrial
        practice in the future. The user acknowledges that during the payment
        through online systems, various third party websites, etc are involved and
        the user is redirected to third party pages for making such payments. The
        company does not manage such third party pages to which the user is
        redirected to. The user recognizes and understands that the user is
        subjected to the terms and conditions, privacy policy, etc on such websites
        and the Company is in no manner liable for making good any losses which the
        user may sustain on such third party pages they are directed to.
    </p>
    <p>
        8.4 You are responsible for the accuracy and authenticity of the
        information provided by You to third-party payment gateways, including the
        bank account number/credit card details and the like. You agree and
        acknowledge that the Company shall not be liable and in no way be held
        responsible for any losses whatsoever, whether direct, indirect, incidental
        or consequential, including without limitation any losses due to delay in
        processing of payment instruction or any credit card fraud.
    </p>
    <p>
        8.5 The subscription amounts paid by the user are inclusive of all the
        applicable taxes as per the relevant laws of the country.
    </p>
    <p>
        8.6 You can reach out to Us for any issue related to payment processing on
        the Website/App, and the same will be forwarded to the concerned
        third-party payment gateway provider for redressal. You can reach out to
        the company at - developer@digitalcelluloid.in The party notes and
        undertakes that the act of the company in forwarding the concerned
        grievances of the user to the third party does not make the company
        involved and/or liable for such losses of the user.
    </p>
    <p>
        <strong>9. LIMITATIONS WHICH MAY BE APPLIED TO ANY USER:</strong>
    </p>
    <p>
        9.1 The Company can limit the usage of the Website and/or the application,
        remove any information, suspend any user from accessing the
        website/application, limit and/or cancel any benefits and/or rewards issued
        to any user, in the event of non-compliance of terms of this Agreement.
    </p>
    <p>
        <strong>
            10. UNDERTAKINGS, WARRANTIES AND REPRESENTATIONS OF THE USER:
        </strong>
    </p>
    <p>
        10.1 The user represents and warrants that the user are either a major or
        is accessing the website/application under the supervision of a legal
        guardian in case of being a minor, at the time of using the Services.
    </p>
    <p>
        10.2 The user undertakes to not use the Website/Application or any of the
        Services or the information of other Users, available on the
        Website/Application.
    </p>
    <p>
        10.3 The user undertakes to not attempt to gain unauthorized access or in
        any way exceed the authorized access to the Website/Application or any
        networks, servers or computer systems connected to the Website/Application
        or Services.
    </p>
    <p>
        10.4 The user shall not conduct any actions detrimental to the interest
        and/or business of the company and its website/business. The user shall not
        damage the reputation and goodwill of the website/application.
    </p>
    <p>
        10.5 the user undertakes to not use the Website/ Application or any of the
        Services or any information on the Website/Application, including any
        Personal Information and Sensitive Personal Information available on the
        Website/ Application, in any unlawful manner, or in a manner which promotes
        or encourages illegal or fraudulent activity, including (without
        limitation) intellectual property infringement or data theft of the
        Content.
    </p>
    <p>
        10.6 The user shall not re-sell, rent, license, or otherwise use any part
        of the Website/ Application or Services to obtain any personal or
        third-party gain.
    </p>
    <p>
        10.7 The user shall not use the company, its affiliates and/or its
        licensors intellectual property, including but not limited to, the
        copyrighted content displayed on the Website and/or App, for any personal
        or third-party gain without the express written consent or authorization of
        the company.
    </p>
    <p>
        10.8 The user shall not conduct any actions which aids or shall not indulge
        in any decipher, decompile, modify, disassemble, adapt, translate or
        reverse engineer any part of the Website and Application, including by
        creating any shadow and mirroring websites.
    </p>
    <p>
        10.9 The user shall not damage, disable, disrupt, impair, create an undue
        burden on, Interfere with, or gain unauthorized access to the Services or
        the Content.
    </p>
    <p>
        10.10 The user undertakes that he has also perused the privacy policy of
        the company herein and is aware and acknowledges that the said policy along
        with the present terms and conditions and/or any other further
        notifications / modifications / circulars / information displayed on the
        website and application shall be read as a part of this terms and
        conditions and shall be referred to as Agreement.
    </p>
    <p>
        10.11 The user undertakes and declares that the user is solely responsible
        for maintaining its secrecy with respect to its login credentials, etc. The
        user undertakes to not to divulge such information to any other person that
        the user himself.
    </p>
    <p>
        10.12 The user undertakes that he shall not access the Website, Application
        and/or its Services, or the personal information of other Users, available
        on the Website/App in order to build a similar or competitive website, app,
        product, or service; and
    </p>
    <p>
        <strong>11. SUPPORT/MAINTAINCE AND RISKS: </strong>
    </p>
    <p>
        11.1 If You have any questions, complaints or claims with respect to the
        Website, App, Content and/or Services, then such correspondence should be
        directed to the contact information provided on the website and
        application. You however acknowledge and agree that the company will have
        no obligation to provide You with any uninterrupted or immediate support or
        maintenance in connection with the Website, Application and/or Services,
        except as provided under the Company’s Privacy Policy.
    </p>
    <p>
        11.2 You represent and warrant that the Account Information, or any other
        information, whether personal or sensitive, provided to Us through an
        email, support forum on the Website, etc. (“Other Information”) does not
        violate any applicable laws or the policy of the company. You may not
        represent or imply to any person or entity, in any way whatsoever, that the
        Account Information or Other Information that You provide Us with is
        sponsored or endorsed by Us in any manner. You alone are responsible for
        Your Account Information. You must note that We are not obligated to create
        a backup copy of Your Account Information or Other Information that You
        provide Us with; We will not be held liable or responsible for the deletion
        of the Account Information or any Other Information that You provide Us
        with.
    </p>
    <p>
        11.3 You agree not to use the Website and/or its Services to collect,
        upload, create, transmit, display, or distribute the Account Information or
        Other Information in any way such: (i) that it violates any third-party
        right, including any copyright, trademark, patent, moral right, privacy
        right, right of publicity, or any other intellectual property or
        proprietary right; or (ii) that it is in violation of any law, regulation,
        or obligations or restrictions imposed by any third party; or (iii) that it
        is hateful, violent, discriminatory, defamatory, racially or ethnically
        objectionable, unlawful, obscene, sexually suggestive, or pornographic.
    </p>
    <p>
        11.4 you agree to not to display, upload, modify, transmit, publish, or
        distribute to, or through, the Website, App or otherwise, any computer
        viruses, worms, or any software codes, files or programs, intended to
        damage, limit or alter a computer system, computer network, or data, and/or
        gain unauthorized access to a computer or computer network;
    </p>
    <p>
        11.5 you agree to not to manipulate the Website to send unsolicited or
        unauthorized advertising, promotional materials, junk mail, spam, chain
        letters, pyramid schemes, or any other form of duplicative or unsolicited
        messages, whether commercial or otherwise;
    </p>
    <p>
        11.6 You agree not to manipulate the Website to harvest, collect, gather,
        or assemble information or data, personal or sensitive regarding any other
        users or individuals, including their e-mail addresses, with or without
        their consent;
    </p>
    <p>
        11.7 you agree to not to interfere with, disrupt, or create an undue burden
        on servers or networks connected to the Website or the data hosted within
        the Website, or violate the regulations, policies, restrictions or
        procedures of such networks;
    </p>
    <p>
        11.8 you agree not to attempt to gain unauthorized access to a third
        party’s information on the Website (or to other computer systems or
        networks connected to or used together with the Website), whether through
        password mining or any other means;
    </p>
    <p>
        11.9 you agree not to harass or interfere with any other user’s use and
        enjoyment of the Website and/or it Services; or use software or automated
        agents or scripts to produce multiple accounts on the Website, or to
        generate automated searches, requests, or queries to (or to strip, scrape,
        or mine data from) the Website.
    </p>
    <p>
        11.10 you agree and acknowledge that the company reserves the right to
        review Your Account Information or any other information from time to time,
        and to investigate and/or take appropriate action against You if you
        violate any applicable laws or the company’s policy, or any other provision
        of these Terms, or otherwise create liability for Us or any other person or
        Entity, in Our sole discretion. The company may take any action including
        removing or modifying Your Profile, terminating Your Profile and/or
        reporting You to, and cooperative with, law enforcement authorities.
    </p>
    <p>
        11.11 you agree and acknowledge that any and all of Your information can be
        shared in the above circumstances with competent authorities upon any
        demand or request by them, or in a good faith belief, that such access,
        preservation, or disclosure is reasonably necessary to: (a) comply with any
        applicable law; (b) enforce the Terms; (c) respond to Your Service requests
        and/or for any customer service; or (d) protect the rights, property or
        personal safety of company, its affiliates or company’s licensors or other
        Users and the public.
    </p>
    <p>
        11.12 you agree that if You provide Us with any feedback or suggestions
        regarding the Website, Content and/or the Services (“Feedback”), You hereby
        assign to Us all rights in such Feedback and agree that We shall have the
        right to use and fully exploit such Feedback and related information in any
        manner We deem appropriate. We will treat any Feedback You provide to Us as
        non-confidential and non-proprietary; therefore, kindly do not submit to Us
        any information or ideas that You consider to be confidential or
        proprietary.
    </p>
    <p>
        <strong>12. INDEMNIFICATION</strong>
    </p>
    <p>
        By using the website, application, services and/or the content, you agree
        to defend and indemnify the company, its affiliates and the company’s
        licensors from any claims, demands, damages, obligations, losses,
        liabilities, costs and/or expenses made against company, its affiliates and
        the company’s licensors by any third parties due to and in relation to any
        act/omission/default of the user, voluntarily and/or out of negligence, for
        violation of the terms herein, for violation of applicable laws and
        regulations,
    </p>
    <p>
        <strong>13. DISCLAIMER OF WARRANTIES: </strong>
    </p>
    <p>
        13.1 The Company and its affiliates, company’s licensors in no manner shall
        be held to have been made any warranties, representations and/or guarantees
        to the user. The use of the website and application by the user is at his
        sole discretion and risk and any loss which may be sustained by the said
        user shall not be attributable to the company, its affiliates, licensors in
        any manner whatsoever. The company does not endorse any content on the
        website/application or assume any responsibility for any product or service
        advertised or offered on the website/application. The company further shall
        not be liable under law, in relation to contents of or use of the
        application/website.
    </p>
    <p>
        13.2 Further, notwithstanding anything contained in the present terms and
        conditions, the company, its affiliates and the licensors in no manner
        shall be liable for any direct, indirect, consequential, exemplary,
        incidental, special or punitive damages, loss of profits, business, data or
        information. All exclusions of limitation of liability shall be applicable
        to the maximum extent permissible under the laws.
    </p>
    <p>
        <strong>14. Third-Party Links &amp; Advertisements</strong>
    </p>
    <p>
        The Website / Application may contain links to third-party links &amp;
        advertisements (“External Links”) that You may access and/or use at Your
        sole discretion. You acknowledge and agree that such External Links are not
        under Our control, and therefore We shall not be responsible, in any
        manner, for any transactions related to External Links. You must note that
        We provide access to these External Links only as a convenience to You, and
        We do not review, approve, monitor, endorse, warrant, or make any
        representations with respect to External Links, in any manner whatsoever.
        You shall use all External Links at Your own risk and should exercise a
        suitable level of caution and discretion in doing so. When You click on any
        of the External Links, the applicable third party’s Terms and policies
        shall apply to You, including the third party’s privacy and data gathering
        practices. You should independently undertake whatever investigation You
        feel necessary or appropriate before proceeding with any transaction in
        connection with such External Links. By using the Services, You acknowledge
        and agree that the company or its affiliates and /or its licensors are not
        responsible or liable to You for any content or other materials hosted and
        served from any website or destination other than the Application or the
        Website.
    </p>
    <p>
        <strong>15. TERM AND TERMINATION</strong>
    </p>
    <p>
        15.1. Subject to this ‘Term and Termination clause’, these Terms will
        remain in full force and effect while You use the Website, Application
        and/or any of its Services, in any way whatsoever. "Upon Termination of a
        Subscription, the You (erstwhile Authorised User) acknowledge that You may
        still be contacted by Us to subscribe to Our Services or for other
        promotional or marketing purposes. You understand that this communication
        may override any Do Not Disturb (DND) registry or DNC or National Customer
        Preference Register (NCPR) that You may have registered for. Your data
        collected at the time of your initial Subscription to the
        website/application shall be subject to the Privacy Policy, available at
        https://www developer@digitalcelluloid.in
    </p>
    <p>
        15.2. We may suspend or terminate Your rights to use the Website and App
        (including Your Account) at any time and for any reason, at Our sole
        discretion, including for any use of the Website and/or its Services in
        violation of these Terms.
    </p>
    <p>
        15.3. Upon termination of Your rights under these Terms, Your Profile and
        right to access and use the Website, App and/or its Services, will
        terminate immediately. You understand that any termination of Your Profile
        may involve deletion of the Account Information and any other Information
        associated with Your Profile from all Our databases. We will not have any
        liability whatsoever to You for any termination of Your rights under these
        Terms, including for termination of Your Profile.
    </p>
    <p>
        <strong>16. GENERAL</strong>
    </p>
    <p>
        16.1 You (a) consent to receive communications from Us in an electronic
        form; and (b) agree that all Terms and conditions, agreements, notices,
        disclosures, and other communications that We provide to You electronically
        satisfy any legal requirement that such communications would satisfy if it
        were to be in a hardcopy writing.
    </p>
    <p>
        16.2. Any waiver of Our rights or remedy under these Terms shall only be
        effective if it is in writing, executed by Our duly authorized
        representative, and shall be applicable only to the circumstances for which
        it is given. Our failure to exercise or enforce any right or remedy under
        these Terms shall not operate as a waiver of such right or remedy, nor
        shall it prevent any future exercise or enforcement of such right or
        remedy. No single or partial exercise of any right or remedy shall preclude
        or restrict the further exercise of any such right, or remedy, or other
        rights or remedies.
    </p>
    <p>
        16.3 Severability - The provision(s) of these Terms shall be sought to be
        harmoniously interpreted with each other, as well as applicable laws
        (including the applicable laws based on the jurisdiction of the User’s),
        and upheld to the fullest extent permissible under applicable laws.
        Further, the provisions contained in this Agreement shall be enforceable
        independent of each other, and their validity shall not be affected, if any
        other provision(s) are held to be invalid. If any provision(s) of these
        Terms are, for any reason, held to be invalid or unenforceable, the other
        provisions of these Terms will be unimpaired (to the maximum extent
        permissible), and the invalid or unenforceable provision(s) will be deemed
        modified so that they are valid and enforceable to the maximum extent
        permitted by applicable laws. Further, if any of those provision(s) are
        void, but would be valid if some part of the provision(s) were deleted, the
        provision(s) in question shall apply with such modification as may be
        necessary to make them valid.
    </p>
    <p>
        16.4. Relationship between You and Us - You confirm that You do not have an
        employment, contractor, agency or partnership relationship with the
        Company. The company is merely providing You the Services on your request.
        Further, You are acting on either on Your own or behalf of another person,
        in the manner stated in the Agreement.
    </p>
    <p>
        16.5 Assignment - These Terms, and Your rights and obligations herein, may
        not be assigned, subcontracted, delegated, or otherwise transferred by You
        without Our prior written consent, and any attempted assignment,
        subcontract, delegation, or transfer in violation of the foregoing will be
        null and void. However, You acknowledge and agree that We reserve the right
        to freely assign Our rights and obligations under these Terms to whomsoever
        We please. The Terms and conditions set forth in these Terms shall be
        binding upon assignees.
    </p>
    <p>
        16.6. Governing Law and Jurisdiction - These Terms and any dispute or claim
        arising out of or in connection with their subject matter or formation
        (including non-contractual disputes or claims) shall be governed by and
        construed in accordance with the laws prevalent in India. You agree that
        the courts of Hyderabad shall have exclusive jurisdiction to settle any
        dispute, or claim, arising out of, or in relation to, these Terms.
    </p>
    <p>
        16.7. Entire Agreement and Amendments -These Terms expressly supersede and
        completely replace any and all prior ‘Terms of Use’ published by the
        company regarding the above subject-matter. The company shall not be bound
        by or liable to You for any pre-existing or contemporaneous written or oral
        representations or warranties, made by anyone, with respect to the Website,
        App, Content and Services, including by any Affiliates of the Company.
    </p>
    <p>
        The company reserves the right, at its sole discretion, to modify the Terms
        from time to time (“Updated Terms of Use”). The Updated Terms of Use shall
        be effective immediately and shall supersede these Terms of Use. The
        company shall not be under an obligation to notify you of any changes to
        the Terms of Use. You shall be solely responsible for reviewing the Terms
        of Use from time to time for any modifications. By continuing to use the
        Website, App, Content and/or Services after the Updated Terms of Use have
        been published, You affirm Your agreement to the Updated Terms of Use
    </p>
    <div>
    </div>
    <p>
        <strong>Last Updated:</strong>
        December 1<sup>st</sup>, 2020.
    </p>
      </div>
      </div>
    
      
    
    </body>
    </html>
    `,
    privacyPolicy: `
    <!DOCTYPE html>
    <html>
    <style type="text/css">
    body{margin:0; padding:0;}
      .bg{
        width: 100%;
        height: initial;
        background-image: url(bg.png);
        background-repeat: no-repeat;
        background-position: center;
        background-size: cover;
      }
      .bg_wrap{
        padding: 40px 30px;
      }
      .bg_wrap h1{
        font-size: 34px;
        color: #fff;
        font-weight: 500;
        display: block;
        text-align: center;
      }
      .bg_wrap p{
        font-size: 16px;
        color: #fff;
      }
      .bg_wrap h2{
        font-size: 26px;
        color: #fff;
      }
      .bg_wrap h3{
        font-size: 22;
        color: #fff;
      }
      .bg_wrap ul{
        margin: 0;
        padding: 0;
      }
      .bg_wrap ul li{
        font-size: 16px;
        color: #fff;
        
      }
      .bg_wrap ol{
        margin: 0;
        padding: 0;
      }
      .bg_wrap ol li{
        font-size: 16px;
        color: #fff;
        
      }
    </style>
    <body>
    
      <div class="bg">
        <div class="bg_wrap">
        <h1>Privacy Policy</h1>
    
    <p>
        <strong><u>BACKGROUND AND INTRODUCTION</u></strong>
    </p>
    <p>
        1) The present privacy policy is enlists and explains the practices adopted
        by Digital Celluloid Pvt. Ltd (hereinafter “Company” or “Friday Movies”)
        for dealing with and handling the information, etc collected by it during
        the course of its customers and/or third parties dealing with the website
        and the mobile application managed by it.
    </p>
    <p>
        2) Digital Celluloid Pvt. Ltd encourages its users to read the terms and
        conditions and the privacy policy in order to be fully informed with
        respect to its rights and obligations while dealing with the present
        website and the mobile application. Information is being is obtained by the
        Digital Celluloid Pvt. Ltd and its affiliates in furtherance of the
        services offered by it through the mobile application and the website.
    </p>
    <p>
        3) Each and every clause of the present privacy policy and the terms and
        conditions and applicable and binding on all the visitors/parties utilizing
        the services and/or those who visit the mobile application and/or the
        website of the company.
    </p>
    <p>
        4) This Privacy Policy must be read along with the Terms and Conditions,
        available at www.fridaymovies.in The Terms of Use is the contract that
        allows you to access the services and content of your choice from us.
    </p>
    <p>
        5) It is explicitly agreed by the customer / visitor that it agrees with
        the terms and conditions as provided at www.fridaymovies.in and the privacy
        policy herein. The company urges the visitors and the customers that in the
        event the customer does not agree with the terms and conditions or the
        privacy policy, the customer and the visitors are urged to not to visit the
        mobile application and the website of the company and/or the services
        rendered by it through the said platforms. In case of any concern and/or
        disagreement, the customer/visitor can contact the company by visiting the
        “Contact Us” Section as provided in the website and the mobile application.
        However, the company reserves its rights to address any concerns raised by
        the customer/visitor.
    </p>
    <p>
        6) We have a customer support team that will address your grievances and
        you may reach out by Mail: developer@digitalcelluloid.in For data related
        issues, users can contact our IT Head/Security officer at:
        developer@digitalcelluloid.in Users can also contact our respective
        Grievance Officer for any kind of grievance related to the content,
        features, offers, subscription and/or payment related queries. Our
        Grievance Officer can be reached at: developer@digitalcelluloid.in
    </p>
    <p>
        7) In the event any customer/visitor raises any concerns with respect to
        the terms and conditions, privacy policy or any services rendered by it,
        including with respect to collection of information and if the
        customer/visitor proceeds to access the services on the mobile application
        and the website in spite of raising any claims/concerns/complaints, it is
        deemed that the visitor/company is agreeable with the present terms and
        conditions and the privacy policy and the services rendered by it through
        the platforms. The company reserves its rights to address any
        concern/complaint raised by the complainant and make any changes at its
        sole discretion to the terms, conditions, privacy policy and/or any other
        services rendered by the company. Irrespective of such resolution of the
        complaint, the complainant is bound by the terms, conditions, privacy
        policy and/or any other conditions imposed by the company.
    </p>
    <p>
        8) It is mandatory that the visitors accessing the services provided by the
        company are of the requisite age as required by the laws of the local
        jurisdiction wherein such services are being obtained by the visitor. It is
        further deemed that the users accessing the services and if underage are
        utilizing the said content only under the supervision and approval of their
        parents and/or legal guardian.
    </p>
    <p>
        9) We request you to review the Privacy Policy from time-to-time. If you
        continue to use the Services after any Privacy Policy amendments, it is
        deemed that you have granted your acceptance of such revisions in the
        privacy Policy. The company at all times retains an unfettered right to
        deny or suspend access to all, or any part of its service to anyone who the
        company reasonably believes has violated any provision of this Privacy
        Policy.
    </p>
    <p>
        10) Please Note that the following information would be collected by the
        company as a part of rendition of services from the visitors/customers:
    </p>
    <p>
        a. The contact information such as name, date of birth, email ID, phone
        number, social media information as provided to us and their respective
        Log-in Ids and/or any other incidental information which may be provided by
        you in furtherance of the your utilization of the services.
    </p>
    <p>
        11) It is declared and informed to the visitor that certain information is
        collected when the visitor/subscriber visits and uses the services as
        rendered by the company. The following is such information:
    </p>
    <p>
        a. The device information of the customer/visitor, the log information, the
        hardware and the model of the device used, Log information, kind, type and
        version of the operating system, unique device identifiers, mobile network
        information, including the geographical location information, usage
        information any other incidental information.
    </p>
    <p>
        b. The subscription details of the visitor/customer, Plan of Subscription,
        Date of Subscription, End date of Subscription, mode and method of payment
        method opted by the customer and/or any other incidental information, The
        API information (the open API Date collected), demographic information
        and/or other information collected for all the affiliates and/or partners
        and/or providers.
    </p>
    <p>
        c. Further, information such as browsing Information, the Watch list, watch
        history, cookies, likes and dislikes, location of streaming, duration of
        streaming, user events, user sessions, type of content you watch, number of
        clicks on our service platform, saved content, downloaded content, watched
        content, Third party marketing agencies and consultancies: Information
        regarding potential subscribers, type of preferred content, etc ,
        information collected from third parties (Third party information/data) are
        collected automatically by the company in furtherance of the process of
        rendition of services.
    </p>
    <p>
        12) It is explicitly informed to the subscribers/viers/visitors/customers
        that certain financial information when shared by such visitor may be
        collected, either by the company itself and/or by third party payment
        gateways, including information such as debit and credit card details
        and/or any other information regarding registration and/or usage of online
        mode of other payments. In the event the visitors have any objection with
        respect to such usage and collection of payment information, the user is
        advised to not to proceed with providing such details to the company.
    </p>
    <p>
        13) The user is informed that the information collected will be used by the
        company for the purposes of providing a viewer experience and allied
        services as rendered by the company to the user; strictly for the purposes
        of business of the company and/or its affiliates and the same shall not be
        shared with any third parties except as provided herein and the terms and
        conditions; to reach out and inform he users and/or prospective users vide
        messages, push notifications, SMS, whatsapp and/or other mode of messages
        and passing of information; to comply with the applicable laws, rules,
        regulations, by-laws, notifications, etc; for verification, upkeep and
        safety of account and the website/mobile application and to rule out any
        violations/fraud, etc; to enhance the rendition of services to the users;
        offer and provide information / services / products / advertisements to the
        users based on the information provided.
    </p>
    <p>
        14) The company does not share the collected information with any third
        party for any purposes other than those provided above. At the same time,
        the company may disclose information such as (I) Users data will be used by
        the marketing team such as emails, in-app, etc; (ii) User ID and personal
        data of users will be used by the customer support team to address any
        issues raised by the users or preventive mechanism, (iii) usage of
        financial data/transaction data by the analytics and financial teams; (iv)
        analysis of user data. IP address, location data and the related data by
        various teams for enhancement of services to the users; (v) to third party
        IT software as service provider who shall have relevant access to
        information; (vi) Third-party payment gateways and other payment service
        providers will be able to access the necessary financial details of the
        users; (vii) any of the information shall be shared for all legal purposes
        by the company to its legal counsels, consultants, etc, including for
        compliances with any laws, investigations, etc
    </p>
    <p>
        and to similar persons (that includes the company members as well as third
        party service providers) to achieve the above-stated purposes:
    </p>
    <p>
        15) The company and the third parties obtaining your information and retain
        your information in a manner so as to identify and keep information only
        for the time period necessary for fulfilling the purposes mentioned under
        this Policy and/or for compliance with any laws and regulations.
    </p>
    <p>
        16) The following are the rights which the user shall have:
    </p>
    <p>
        a. You can always choose not to provide the requested information to us,
        even though it may result you in not availing certain features as provided
        by the company and its affiliates.
    </p>
    <p>
        b. You may access information that the company holds and raise any request
        for rectifying, modifying or deleting personal information by raising a
        request to our customer support/grievance officer, with the necessary
        contact details provided above. The company shall make efforts to make such
        modification and/or corrections.
    </p>
    <p>
        c. If you think that any of your personal information that we hold is
        inappropriate/incorrect/inaccurate or delete the same, and you want us to
        rectify the same, you can approach us at ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
        or reach us out at ……………………….
    </p>
    <p>
        d. You may add further information and/or edit the information provided by
        you.
    </p>
    <p>
        e. If you want to withdraw your consent or raise any objection to the use
        of your information for receiving any direct marketing information to which
        you previously opted-in, you can do so by unsubscribing to such direct
        marketing by contacting our customer support at above mentioned addresses.
        If you withdraw/object your consent, our use of your information before you
        withdrawal shall be valid and binding upon you.
    </p>
    <p>
        17) It is declared that the company as well as third parties obtaining
        information are committed to ensuring that the personal information is
        protected and secure with the company. In order to prevent unauthorized
        access or divulgence of your personal information, we, as well as third
        parties obtaining your information, have put in place suitable technical
        and organizational measures to safeguard and secure the personal
        information: the following are a few measures:
    </p>
    <p>
        a. The company performs periodical “Data Protection Impact Assessment” to
        identify risks to user information and “Data Protection Compliance Reviews”
        to ensure those risks are addressed.
    </p>
    <p>
        b. Compliances in accordance with all the applicable laws, rules and
        regulations as required under the jurisdiction of the country wherein the
        company is registered.
    </p>
    <p>
        c. Encryption of information in the form by Secure Server Software (SSL)
        before it is sent to the company and other third parties.
    </p>
    <p>
        d. Place safeguards against phishing, theft, etc. The company does not
        disclose and give details of credit card, or debit card, PIN in a
        non-secure, or unsolicited e-mail, or telephone communication.
    </p>
    <p>
        18) The company uses the support services of third-party platforms and/or
        companies to direct you to payment gateways when you opt to pay for our
        Services. Your financial information is collected, stored and retained by
        such third-party platforms as per the terms and policies of such third
        parties. We and such third-party platforms undertake measures designed to
        provide a security level that is appropriate to the risks of processing
        your personal information. You are therefore mandatorily required to check
        and consent to the “Privacy Policy” of such third-party platforms in order
        to accept how such third-party platforms handle your information.
    </p>
    <p>
        19) That in the event the user/customer has any grievance, they can reach
        out to the company vide developer@digitalcelluloid.in The company does not
        and shall not resort to resolve any issue and/or grievance raised by the
        user/subscriber which deals with any issues pertaining to third parties
        and/or other entities.
    </p>
    <p>
        20) In the event of any grievance and/or dispute that may be raised by the
        user/subscriber against the company and/or affiliates and/or third parties
        relating to the services rendered by the company and/or affiliates and/or
        third parties, the same shall be raised within the exclusive jurisdiction
        of the courts in HYDERABAD only. No other court shall have jurisdiction on
        any issues arising out of and under the services provided by the company
        and/or its affiliates and/or third parties relating the present services.
    </p>
    <p>
        21) The user undertakes and declares that he has read all the terms and
        conditions, legal disclaimers, the present privacy policy and/or any other
        disclaimers, information, etc provided by the company with respect to the
        services rendered by it through the website and the mobile application.
    </p>
    <p align="center">
        ***
    </p>
      </div>
      </div>
    
      
    
    </body>
    </html>
    `,
  },
  fr: {
    hello: 'Bonjour',
  },
});

export default Strings;
