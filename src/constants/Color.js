const Color = {
  BACKGROUND_COLOR: '#222222',
  ICON_COLOR: '#777777',
  APP_YELLOW: '#faec4e',
};
export default Color;
