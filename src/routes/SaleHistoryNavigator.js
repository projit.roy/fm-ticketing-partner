import {createStackNavigator} from '@react-navigation/stack';
import React, {Component} from 'react';
import normalize from 'react-native-normalize';
import Color from '../constants/Color';
import {View, Image, TouchableOpacity, StyleSheet, Text} from 'react-native';
//import OrderHistory from '../containers/OrderHistory';
import Icon from 'react-native-easy-icon';
import SaleHistory from '../screens/SaleHistory';

const Stack = createStackNavigator();

export default class OrderHistoryNavigator extends Component {
  render() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: Color.BACKGROUND_COLOR,
          },
          headerTintColor: 'white',
          headerTitleStyle: {fontWeight: 'bold'},
          headerLeft: () => (
            <View style={styles.container}>
              <TouchableOpacity
                style={styles.buttonStyle}
                onPress={() => {
                  this.props.navigation.goBack();
                }}>
                <Icon
                  name={'chevron-left'}
                  type="material-community"
                  size={normalize(30)}
                  color="white"
                />
              </TouchableOpacity>
              <Text
                style={{
                  fontWeight: 'bold',
                  color: 'white',
                  fontSize: normalize(22),
                }}>
                Sale History
              </Text>
            </View>
          ),
          headerRight: () => (
            <View style={styles.container}>
              <TouchableOpacity style={styles.buttonStyle} />
            </View>
          ),
        }}>
        <Stack.Screen
          name="sale"
          component={SaleHistory}
          options={{title: ''}}
        />
      </Stack.Navigator>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    //  flex: 1,
    marginStart: normalize(5),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  title: {
    padding: normalize(5),
    fontSize: normalize(16),
    color: 'white',
  },
  buttonStyle: {
    padding: normalize(5),
  },
  headerStyle: {
    marginStart: normalize(5),
    justifyContent: 'center',
    alignItems: 'center',
  },
});
