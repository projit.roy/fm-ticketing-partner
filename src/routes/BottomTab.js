import Settings from '../screens/Settings';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import * as React from 'react';
import Tickets from '../screens/Tickets';
import Sale from '../screens/Sale';
import Dashboard from '../screens/Dashboard';
import {Image} from 'react-native';
import text from '../constants/strings';

const Tab = createBottomTabNavigator();

// export type AppTabParamList = {
//   Home: undefined;
//   Settings: {userID?: string};
// };

const App = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        safeAreaInsets: {bottom: 0, top: 0},
        activeTintColor: '#fff',
        //  inactiveTintColor: 'lightgray',
        // activeBackgroundColor: '#c4461c',
        // inactiveBackgroundColor: '#b55031',
        style: {
          backgroundColor: '#000',
          paddingBottom: 3,
        },
      }}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName = {};

          if (route.name === text.dashboard) {
            iconName = focused
              ? require('../../assets/home_white.png')
              : require('../../assets/home_grey_Copy.png');
          } else if (route.name === text.account) {
            iconName = focused
              ? require('../../assets/setting.png')
              : require('../../assets/setting-Copy.png');
          } else if (route.name === text.sale) {
            iconName = focused
              ? require('../../assets/tag.png')
              : require('../../assets/tag_Copy.png');
          } else if (route.name === text.ticket) {
            iconName = focused
              ? require('../../assets/gift_white.png')
              : require('../../assets/cinema.png');
          }

          // You can return any component that you like here!
          return <Image source={iconName} />;
        },
      })}>
      <Tab.Screen name={text.dashboard} component={Dashboard} />
      <Tab.Screen name={text.ticket} component={Tickets} />
      <Tab.Screen name={text.sale} component={Sale} />
      <Tab.Screen name={text.account} component={Settings} />
    </Tab.Navigator>
  );
};

export default App;
