import {createStackNavigator} from '@react-navigation/stack';
import React, {Component} from 'react';
import Login from '../screens/Login';
import Register from '../screens/Register';
import ResetPassword from '../screens/ResetPassword';
import VerifyOtp from '../screens/VerifyOtp';
import {SafeAreaProvider, SafeAreaView} from 'react-native-safe-area-context';

const Stack = createStackNavigator();

export default class LoginNavigator extends Component {
  render() {
    return (
      <SafeAreaProvider>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}>
          <Stack.Screen
            name="LoginScreen"
            component={Login}
            options={{title: 'Login'}}
          />
          <Stack.Screen
            name="Register"
            component={Register}
            options={{title: 'Register'}}
          />
          <Stack.Screen
            name="ResetPassword"
            component={ResetPassword}
            options={{title: 'ResetPassword'}}
          />
          <Stack.Screen
            name="VerifyOTP"
            component={VerifyOtp}
            options={{title: 'VerifyOTP'}}
          />
        </Stack.Navigator>
      </SafeAreaProvider>
    );
  }
}
